-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 09, 2020 at 12:21 PM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.3.21-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `butikfitrah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_penjualan`
--

CREATE TABLE `tb_detail_penjualan` (
  `id_detail_penjualan` int(10) UNSIGNED NOT NULL,
  `id_penjualan` int(10) UNSIGNED NOT NULL,
  `id_produk` int(10) UNSIGNED NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `harga_modal` int(10) UNSIGNED NOT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `subtotal` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_penjualan`
--

INSERT INTO `tb_detail_penjualan` (`id_detail_penjualan`, `id_penjualan`, `id_produk`, `jumlah`, `harga_modal`, `harga`, `subtotal`) VALUES
(1, 1, 1, 1, 80000, 120000, 120000),
(2, 2, 35, 3, 80000, 120000, 360000),
(3, 3, 35, 1, 80000, 120000, 120000),
(4, 5, 35, 1, 80000, 120000, 120000),
(5, 8, 35, 1, 80000, 120000, 120000),
(6, 10, 35, 1, 80000, 120000, 120000),
(7, 12, 35, 1, 80000, 120000, 120000),
(8, 16, 35, 1, 80000, 120000, 120000),
(9, 19, 35, 1, 80000, 120000, 120000),
(10, 22, 35, 1, 80000, 120000, 120000),
(11, 24, 35, 2, 80000, 120000, 240000),
(12, 25, 35, 2, 80000, 120000, 240000),
(13, 26, 34, 3, 80000, 120000, 360000),
(14, 26, 32, 2, 80000, 120000, 240000),
(15, 27, 31, 1, 80000, 120000, 120000),
(16, 27, 34, 1, 80000, 120000, 120000),
(17, 30, 32, 1, 80000, 120000, 120000),
(18, 31, 32, 1, 80000, 120000, 120000),
(19, 32, 32, 1, 80000, 120000, 120000),
(20, 34, 1, 1, 80000, 120000, 120000),
(21, 36, 1, 1, 80000, 120000, 120000),
(22, 37, 2, 1, 80000, 120000, 120000),
(23, 39, 36, 30, 69300, 99000, 2970000),
(24, 40, 36, 3, 69300, 99000, 297000),
(25, 41, 35, 1, 80000, 120000, 120000),
(26, 41, 32, 1, 80000, 120000, 120000),
(27, 42, 32, 1, 80000, 120000, 120000),
(28, 44, 34, 1, 80000, 120000, 120000),
(29, 45, 34, 1, 80000, 120000, 120000),
(30, 46, 36, 1, 69300, 99000, 99000),
(31, 47, 35, 12, 80000, 120000, 1440000),
(32, 48, 35, 12, 80000, 120000, 1440000),
(33, 49, 35, 2, 80000, 120000, 240000),
(34, 50, 35, 1, 80000, 120000, 120000),
(35, 52, 37, 1, 200000, 500000, 500000),
(36, 54, 37, 1, 200000, 500000, 500000),
(37, 56, 37, 1, 200000, 500000, 500000),
(38, 56, 37, 1, 200000, 500000, 500000),
(39, 57, 37, 1, 200000, 500000, 500000),
(40, 57, 36, 1, 69300, 99000, 99000),
(41, 59, 37, 1, 200000, 500000, 500000),
(42, 60, 37, 1, 200000, 500000, 500000),
(43, 61, 37, 1, 200000, 500000, 500000),
(44, 62, 37, 1, 200000, 500000, 500000),
(45, 63, 37, 1, 200000, 500000, 500000),
(46, 64, 37, 1, 200000, 500000, 500000),
(47, 65, 37, 1, 200000, 500000, 500000),
(48, 66, 37, 1, 200000, 500000, 500000),
(49, 67, 37, 1, 200000, 500000, 500000),
(50, 68, 37, 3, 200000, 500000, 1500000),
(51, 69, 37, 1, 200000, 500000, 500000),
(52, 70, 37, 1, 200000, 500000, 500000),
(53, 73, 37, 1, 200000, 500000, 500000),
(54, 75, 37, 1, 200000, 500000, 500000),
(55, 86, 37, 1, 200000, 500000, 500000),
(56, 87, 37, 1, 200000, 500000, 500000),
(57, 89, 37, 1, 200000, 500000, 500000),
(58, 91, 37, 1, 200000, 500000, 500000),
(59, 93, 37, 1, 200000, 500000, 500000),
(60, 95, 37, 1, 200000, 500000, 500000),
(61, 97, 37, 1, 200000, 500000, 500000),
(62, 99, 37, 1, 200000, 500000, 500000),
(63, 101, 37, 1, 200000, 500000, 500000),
(64, 102, 37, 1, 200000, 500000, 500000),
(65, 103, 37, 1, 200000, 500000, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_dipesan`
--

CREATE TABLE `tb_dipesan` (
  `id_pesanan` int(10) UNSIGNED NOT NULL,
  `id_produk` int(10) UNSIGNED NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `jatuh_tempo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dipesan`
--

INSERT INTO `tb_dipesan` (`id_pesanan`, `id_produk`, `jumlah`, `jatuh_tempo`) VALUES
(1, 1, 1, '2018-04-26'),
(2, 35, 3, '2018-04-27'),
(3, 35, 1, '2018-04-28'),
(4, 35, 1, '2018-04-28'),
(5, 35, 1, '2018-04-28'),
(6, 35, 1, '2018-04-28'),
(7, 35, 1, '2018-04-28'),
(8, 35, 1, '2018-04-28'),
(9, 35, 1, '2018-04-28'),
(10, 35, 1, '2018-04-28'),
(11, 35, 2, '2018-05-10'),
(12, 35, 2, '2018-05-10'),
(13, 34, 3, '2018-05-10'),
(14, 32, 2, '2018-05-10'),
(15, 31, 1, '2018-06-05'),
(16, 34, 1, '2018-06-05'),
(17, 32, 1, '2018-06-05'),
(18, 32, 1, '2018-06-05'),
(19, 32, 1, '2018-07-05'),
(20, 1, 1, '2018-07-05'),
(21, 1, 1, '2018-07-05'),
(22, 2, 1, '2018-07-05'),
(23, 36, 30, '2018-07-06'),
(24, 36, 3, '2018-07-08'),
(25, 35, 1, '2018-07-16'),
(26, 32, 1, '2018-07-16'),
(27, 32, 1, '2018-07-16'),
(28, 34, 1, '2018-07-16'),
(29, 34, 1, '2018-07-16'),
(30, 36, 1, '2018-07-28'),
(31, 35, 12, '2019-10-19'),
(32, 35, 12, '2019-10-19'),
(33, 35, 2, '2019-10-19'),
(34, 35, 1, '2020-08-27'),
(35, 37, 1, '2020-08-27'),
(36, 37, 1, '2020-08-27'),
(37, 37, 1, '2020-08-27'),
(38, 37, 1, '2020-08-27'),
(39, 37, 1, '2020-08-27'),
(40, 36, 1, '2020-08-27'),
(41, 37, 1, '2020-08-27'),
(42, 37, 1, '2020-08-27'),
(43, 37, 1, '2020-08-27'),
(44, 37, 1, '2020-08-27'),
(45, 37, 1, '2020-08-28'),
(46, 37, 1, '2020-08-28'),
(47, 37, 1, '2020-08-28'),
(48, 37, 1, '2020-08-28'),
(49, 37, 1, '2020-08-28'),
(50, 37, 3, '2020-09-01'),
(51, 37, 1, '2020-09-09'),
(52, 37, 1, '2020-09-09'),
(53, 37, 1, '2020-09-09'),
(54, 37, 1, '2020-09-09'),
(55, 37, 1, '2020-09-09'),
(56, 37, 1, '2020-09-09'),
(57, 37, 1, '2020-09-09'),
(58, 37, 1, '2020-09-10'),
(59, 37, 1, '2020-09-10'),
(60, 37, 1, '2020-09-10'),
(61, 37, 1, '2020-09-10'),
(62, 37, 1, '2020-09-10'),
(63, 37, 1, '2020-09-10'),
(64, 37, 1, '2020-09-10'),
(65, 37, 1, '2020-09-10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gambar`
--

CREATE TABLE `tb_gambar` (
  `id_gambar` int(10) UNSIGNED NOT NULL,
  `id_produk` int(10) NOT NULL,
  `nama_gambar` varchar(37) NOT NULL,
  `main_image` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_gambar`
--

INSERT INTO `tb_gambar` (`id_gambar`, `id_produk`, `nama_gambar`, `main_image`) VALUES
(2, 33, '7ac8161a20ba78c4bc8616b7dcdc235d.jpg', 1),
(3, 35, '8f628f8e2e43be931f5e676c7ecdef54.jpg', 1),
(4, 34, '0c0bfad35900d4d14fb164053b8ff741.jpg', 1),
(5, 37, '9395bcc8a1d7a29607969dbbdff8cf71.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(10) UNSIGNED NOT NULL,
  `nama_kategori` varchar(20) NOT NULL,
  `jenis_kategori` tinyint(3) UNSIGNED NOT NULL COMMENT '1 = men clothing, 2 = men shoes, 3 = men accesories, 4 = men look and trends, 5 = women clothing, 6 = women shoes, 7 = women accesories, 8 = women look and trends'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`, `jenis_kategori`) VALUES
(1, 'T-shirts', 0),
(2, 'Baju koko', 1),
(3, 'Celana panjang', 0),
(4, 'Jam tangan', 1),
(5, 'Gelang', 1),
(6, 'Kaftan & Gamis', 0),
(7, 'Pashmina', 0),
(8, 'Hijab Scarf', 0),
(11, 'test', 0),
(12, 'test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_komentar`
--

CREATE TABLE `tb_komentar` (
  `id_komentar` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `komentar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id_pembayaran` int(10) UNSIGNED NOT NULL,
  `total_harga` int(10) UNSIGNED NOT NULL,
  `total_quantity` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `id_penjualan` int(10) UNSIGNED NOT NULL,
  `kode_unik` smallint(5) UNSIGNED NOT NULL,
  `jumlah_dibayar` int(10) UNSIGNED NOT NULL,
  `tanggal_berakhir` datetime NOT NULL,
  `bank` varchar(10) NOT NULL,
  `no_rekening` varchar(30) NOT NULL,
  `nama_pengirim` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `status_pembayaran` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0=belum confirm, 1=sudah confirm, 2=sudah verifikasi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `total_harga`, `total_quantity`, `customer_id`, `id_penjualan`, `kode_unik`, `jumlah_dibayar`, `tanggal_berakhir`, `bank`, `no_rekening`, `nama_pengirim`, `tanggal`, `jumlah`, `status_pembayaran`) VALUES
(1, 120000, 1, 2, 1, 24, 120024, '2018-04-26 00:00:00', 'asdf', '123456', '12', '2018-12-12', 120024, 2),
(2, 360000, 3, 2, 2, 341, 360341, '2018-07-27 00:00:00', '', '', '', '0000-00-00', 0, 0),
(3, 120000, 1, 2, 3, 669, 120669, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(4, 0, 0, 2, 4, 109, 109, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(5, 120000, 1, 2, 5, 960, 120960, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(6, 0, 0, 2, 6, 10, 10, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(7, 0, 0, 2, 7, 862, 862, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(8, 120000, 1, 2, 8, 11, 120011, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(9, 0, 0, 2, 9, 550, 550, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(10, 120000, 1, 2, 10, 907, 120907, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(11, 0, 0, 2, 11, 128, 128, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(12, 120000, 1, 2, 12, 204, 120204, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(13, 0, 0, 2, 13, 769, 769, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(14, 0, 0, 2, 14, 164, 164, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(15, 0, 0, 2, 15, 126, 126, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(16, 120000, 1, 2, 16, 44, 120044, '2018-04-28 00:00:00', 'BCA', '1234', 'yayan', '2018-12-12', 120044, 2),
(17, 0, 0, 2, 17, 654, 654, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(18, 0, 0, 2, 18, 118, 118, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(19, 120000, 1, 2, 19, 214, 120214, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(20, 0, 0, 2, 20, 71, 71, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(21, 0, 0, 2, 21, 304, 304, '2018-04-28 00:00:00', 'BCA', '1234', 'Yayan', '2018-12-12', 304, 2),
(22, 120000, 1, 2, 22, 187, 120187, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(23, 0, 0, 2, 23, 556, 556, '2018-04-28 00:00:00', '', '', '', '0000-00-00', 0, 0),
(24, 240000, 2, 2, 25, 753, 240753, '2018-05-10 00:00:00', '', '', '', '0000-00-00', 0, 0),
(25, 600000, 5, 2, 26, 530, 600530, '2018-05-10 00:00:00', '', '', '', '0000-00-00', 0, 0),
(26, 240000, 2, 2, 27, 259, 240259, '2018-06-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(27, 0, 0, 2, 28, 922, 922, '2018-06-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(28, 120000, 1, 2, 30, 468, 120468, '2018-06-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(29, 120000, 1, 2, 31, 575, 120575, '2018-06-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(30, 120000, 1, 2, 32, 524, 120524, '2018-07-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(31, 120000, 1, 2, 34, 7, 120007, '2018-07-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(32, 120000, 1, 2, 36, 691, 120691, '2018-07-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(33, 120000, 1, 2, 37, 29, 120029, '2018-07-05 00:00:00', '', '', '', '0000-00-00', 0, 0),
(34, 2970000, 30, 2, 39, 462, 2970462, '2018-07-06 00:00:00', '', '', '', '0000-00-00', 0, 0),
(35, 297000, 3, 2, 40, 693, 297693, '2018-07-08 00:00:00', 'BNI', '07512351', 'Harselita', '2018-07-05', 297693, 2),
(36, 262897, 2, 2, 41, 897, 262897, '2018-07-13 08:43:35', '', '', '', '0000-00-00', 0, 0),
(37, 142186, 1, 2, 42, 186, 142186, '2018-07-13 08:44:10', '', '', '', '0000-00-00', 0, 0),
(38, 142053, 1, 2, 44, 53, 142053, '2018-07-13 08:50:23', '', '', '', '0000-00-00', 0, 0),
(39, 142924, 1, 2, 45, 924, 142924, '2018-07-13 09:56:35', '', '', '', '0000-00-00', 0, 0),
(40, 179732, 1, 16, 46, 732, 179732, '2018-07-25 10:56:29', '', '', '', '0000-00-00', 0, 0),
(41, 300469, 2, 2, 49, 469, 300469, '2019-10-19 22:24:06', 'test', '1234', 'test', '1970-01-01', 300469, 2),
(42, 120553, 1, 1, 50, 553, 120553, '2020-08-27 22:53:23', '', '', '', '0000-00-00', 0, 0),
(43, 500591, 1, 1, 52, 591, 500591, '2020-08-27 23:10:41', '', '', '', '0000-00-00', 0, 0),
(44, 500754, 1, 1, 54, 754, 500754, '2020-08-27 23:19:39', '', '', '', '0000-00-00', 0, 0),
(45, 1000629, 2, 1, 56, 629, 1000629, '2020-08-27 23:21:25', '', '', '', '0000-00-00', 0, 0),
(46, 599740, 2, 1, 57, 740, 599740, '2020-08-27 23:30:32', '', '', '', '0000-00-00', 0, 0),
(47, 500152, 1, 1, 59, 152, 500152, '2020-08-27 23:31:59', '', '', '', '0000-00-00', 0, 0),
(48, 500283, 1, 1, 60, 283, 500283, '2020-08-27 23:32:40', '', '', '', '0000-00-00', 0, 0),
(49, 500811, 1, 1, 61, 811, 500811, '2020-08-27 23:33:03', '', '', '', '0000-00-00', 0, 0),
(50, 566982, 1, 16, 62, 982, 566982, '2020-08-27 23:36:07', '', '', '', '0000-00-00', 0, 0),
(51, 566544, 1, 16, 63, 544, 566544, '2020-08-28 06:45:32', '', '', '', '0000-00-00', 0, 0),
(52, 566778, 1, 16, 64, 778, 566778, '2020-08-28 06:57:39', '', '', '', '0000-00-00', 0, 0),
(53, 566276, 1, 16, 65, 276, 566276, '2020-08-28 07:10:40', '', '', '', '0000-00-00', 0, 0),
(54, 566835, 1, 16, 66, 835, 566835, '2020-08-28 07:15:34', '', '', '', '0000-00-00', 0, 0),
(55, 566547, 1, 16, 67, 547, 566547, '2020-08-28 07:46:27', '', '', '', '0000-00-00', 0, 0),
(56, 1566260, 3, 16, 68, 260, 1566260, '2020-09-01 08:34:06', '', '', '', '0000-00-00', 0, 0),
(57, 566324, 1, 16, 69, 324, 566324, '2020-09-08 13:52:28', '', '', '', '0000-00-00', 0, 0),
(58, 566575, 1, 16, 70, 575, 566575, '2020-09-08 22:57:40', '', '', '', '0000-00-00', 0, 0),
(59, 566445, 1, 16, 73, 445, 566445, '2020-09-08 23:08:29', '', '', '', '0000-00-00', 0, 0),
(60, 566180, 1, 16, 75, 180, 566180, '2020-09-08 23:09:56', '', '', '', '0000-00-00', 0, 0),
(61, 734, 0, 16, 78, 734, 734, '2020-09-09 22:51:49', '', '', '', '0000-00-00', 0, 0),
(62, 373, 0, 16, 80, 373, 373, '2020-09-09 22:53:04', '', '', '', '0000-00-00', 0, 0),
(63, 791, 0, 16, 82, 791, 791, '2020-09-09 22:56:39', '', '', '', '0000-00-00', 0, 0),
(64, 798, 0, 16, 83, 798, 798, '2020-09-09 23:08:10', '', '', '', '0000-00-00', 0, 0),
(65, 220, 0, 16, 85, 220, 220, '2020-09-09 23:09:35', '', '', '', '0000-00-00', 0, 0),
(66, 555459, 1, 16, 86, 459, 555459, '2020-09-09 23:10:22', '', '', '', '0000-00-00', 0, 0),
(67, 555213, 1, 16, 87, 213, 555213, '2020-09-09 23:13:06', '', '', '', '0000-00-00', 0, 0),
(68, 547693, 1, 16, 89, 693, 547693, '2020-09-09 23:14:07', '', '', '', '0000-00-00', 0, 0),
(69, 552864, 1, 16, 91, 864, 552864, '2020-09-10 10:59:42', '', '', '', '0000-00-00', 0, 0),
(70, 500730, 1, 16, 97, 730, 500730, '2020-09-10 12:02:09', '', '', '', '0000-00-00', 0, 0),
(71, 500457, 1, 16, 99, 457, 500457, '2020-09-10 12:03:55', '', '', '', '0000-00-00', 0, 0),
(72, 500223, 1, 16, 101, 223, 500223, '2020-09-10 12:05:50', '', '', '', '0000-00-00', 0, 0),
(73, 500324, 1, 16, 102, 324, 500324, '2020-09-10 12:11:31', '', '', '', '0000-00-00', 0, 0),
(74, 606713, 1, 16, 103, 713, 606713, '2020-09-10 12:12:09', '', '', '', '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengiriman`
--

CREATE TABLE `tb_pengiriman` (
  `id_pengiriman` int(10) UNSIGNED NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `id_provinsi` int(10) UNSIGNED NOT NULL,
  `id_kota` int(10) UNSIGNED NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `kurir` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengiriman`
--

INSERT INTO `tb_pengiriman` (`id_pengiriman`, `nama_penerima`, `id_provinsi`, `id_kota`, `telepon`, `kecamatan`, `kelurahan`, `alamat`, `kurir`) VALUES
(1, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(2, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(3, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(4, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(5, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(6, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(7, 'Lita', 1, 277, '1234', '1', '1', 'test', 'jne'),
(8, 'Lita', 1, 277, '1234', '1', '1', 'test', 'jne'),
(9, 'Lita', 1, 16, '1234', '1', '1', 'test', 'jne'),
(10, 'Lita', 1, 12, '1234', '1', '1', 'test', 'jne'),
(11, 'Lita', 1, 18, '1234', '1', '1', 'test', 'jne'),
(12, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(13, 'Lita', 1, 13, '1234', '1', '1', 'test', 'jne'),
(14, 'Lita', 1, 13, '1234', '1', '1', 'test', 'jne'),
(15, 'Lita', 1, 1, '1234', '1', '1', 'test', 'jne'),
(16, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(17, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(18, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(19, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(20, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(21, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(22, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(23, 'Lita', 1, 1, '1234', '1', '1', 'test', 'pos'),
(24, 'Lita', 1, 1, '1234', '1', '1', 'test', 'tiki');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `id_penjualan` int(11) UNSIGNED NOT NULL,
  `tanggal_penjualan` date NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `total_quantity` int(10) UNSIGNED NOT NULL,
  `total_harga` int(10) UNSIGNED NOT NULL,
  `ongkos_kirim` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0=belum proses, 1=sudah proses',
  `resi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`id_penjualan`, `tanggal_penjualan`, `customer_id`, `total_quantity`, `total_harga`, `ongkos_kirim`, `total`, `status`, `resi`) VALUES
(1, '2018-04-23', 2, 1, 120000, 0, 120000, 1, '12345'),
(2, '2018-04-24', 1, 3, 360000, 0, 360000, 0, ''),
(3, '2018-04-25', 1, 1, 120000, 0, 120000, 0, ''),
(4, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(5, '2018-04-25', 1, 1, 120000, 0, 120000, 0, ''),
(6, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(7, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(8, '2018-04-25', 1, 1, 120000, 0, 120000, 0, ''),
(9, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(10, '2018-04-25', 1, 1, 120000, 0, 120000, 0, ''),
(11, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(12, '2018-04-25', 1, 1, 120000, 0, 120000, 0, ''),
(13, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(14, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(15, '2018-04-25', 1, 0, 0, 0, 0, 0, ''),
(16, '2018-04-25', 2, 1, 120000, 22000, 142000, 1, '123456'),
(17, '2018-04-25', 2, 0, 0, 0, 0, 0, ''),
(18, '2018-04-25', 2, 0, 0, 0, 0, 0, ''),
(19, '2018-04-25', 2, 1, 120000, 22000, 142000, 0, ''),
(20, '2018-04-25', 2, 0, 0, 0, 0, 0, ''),
(21, '2018-04-25', 2, 0, 0, 0, 0, 1, '12345'),
(22, '2018-04-25', 2, 1, 120000, 22000, 142000, 0, ''),
(23, '2018-04-25', 2, 0, 0, 0, 0, 0, ''),
(24, '2018-05-07', 1, 0, 0, 0, 0, 0, ''),
(25, '2018-05-07', 1, 2, 240000, 0, 240000, 0, ''),
(26, '2018-05-07', 2, 5, 600000, 22000, 622000, 0, ''),
(27, '2018-06-02', 2, 2, 240000, 22000, 262000, 0, ''),
(28, '2018-06-02', 2, 0, 0, 0, 0, 0, ''),
(29, '2018-06-02', 2, 0, 0, 0, 0, 0, ''),
(30, '2018-06-02', 2, 1, 120000, 22000, 142000, 0, ''),
(31, '2018-06-02', 2, 1, 120000, 22000, 142000, 0, ''),
(32, '2018-07-02', 1, 1, 120000, 0, 120000, 0, ''),
(33, '2018-07-02', 1, 0, 0, 0, 0, 0, ''),
(34, '2018-07-02', 1, 1, 120000, 0, 120000, 0, ''),
(35, '2018-07-02', 1, 0, 0, 0, 0, 0, ''),
(36, '2018-07-02', 1, 1, 120000, 0, 120000, 0, ''),
(37, '2018-07-02', 2, 1, 120000, 22000, 142000, 0, ''),
(38, '2018-07-02', 2, 0, 0, 0, 0, 0, ''),
(39, '2018-07-03', 2, 30, 2970000, 132000, 3102000, 0, ''),
(40, '2018-07-05', 2, 3, 297000, 22000, 319000, 1, '12345'),
(41, '2018-07-13', 2, 2, 240000, 22000, 262000, 0, ''),
(42, '2018-07-13', 2, 1, 120000, 22000, 142000, 0, ''),
(43, '2018-07-13', 2, 0, 0, 0, 0, 0, ''),
(44, '2018-07-13', 2, 1, 120000, 22000, 142000, 0, ''),
(45, '2018-07-13', 2, 1, 120000, 22000, 142000, 0, ''),
(46, '2018-07-25', 16, 1, 99000, 80000, 179000, 0, ''),
(47, '2019-10-19', 2, 0, 0, 0, 0, 0, ''),
(48, '2019-10-19', 2, 0, 0, 0, 0, 0, ''),
(49, '2019-10-19', 2, 2, 240000, 60000, 300000, 1, '12345'),
(50, '2020-08-27', 1, 1, 120000, 0, 120000, 0, ''),
(51, '2020-08-27', 1, 0, 0, 0, 0, 0, ''),
(52, '2020-08-27', 1, 1, 500000, 0, 500000, 0, ''),
(53, '2020-08-27', 1, 0, 0, 0, 0, 0, ''),
(54, '2020-08-27', 1, 1, 500000, 0, 500000, 0, ''),
(55, '2020-08-27', 1, 0, 0, 0, 0, 0, ''),
(56, '2020-08-27', 1, 2, 1000000, 0, 1000000, 0, ''),
(57, '2020-08-27', 1, 2, 599000, 0, 599000, 0, ''),
(58, '2020-08-27', 1, 0, 0, 0, 0, 0, ''),
(59, '2020-08-27', 1, 1, 500000, 0, 500000, 0, ''),
(60, '2020-08-27', 1, 1, 500000, 0, 500000, 0, ''),
(61, '2020-08-27', 1, 1, 500000, 0, 500000, 0, ''),
(62, '2020-08-27', 16, 1, 500000, 66000, 566000, 0, ''),
(63, '2020-08-28', 16, 1, 500000, 66000, 566000, 0, ''),
(64, '2020-08-28', 16, 1, 500000, 66000, 566000, 0, ''),
(65, '2020-08-28', 16, 1, 500000, 66000, 566000, 0, ''),
(66, '2020-08-28', 16, 1, 500000, 66000, 566000, 0, ''),
(67, '2020-08-28', 16, 1, 500000, 66000, 566000, 0, ''),
(68, '2020-09-01', 16, 3, 1500000, 66000, 1566000, 0, ''),
(69, '2020-09-08', 16, 1, 500000, 66000, 566000, 0, ''),
(70, '2020-09-08', 16, 1, 500000, 66000, 566000, 0, ''),
(71, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(72, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(73, '2020-09-08', 16, 1, 500000, 66000, 566000, 0, ''),
(74, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(75, '2020-09-08', 16, 1, 500000, 66000, 566000, 0, ''),
(76, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(77, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(78, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(79, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(80, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(81, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(82, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(83, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(84, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(85, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(86, '2020-09-08', 16, 1, 500000, 55000, 555000, 0, ''),
(87, '2020-09-08', 16, 1, 500000, 55000, 555000, 0, ''),
(88, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(89, '2020-09-08', 16, 1, 500000, 47000, 547000, 0, ''),
(90, '2020-09-08', 16, 0, 0, 0, 0, 0, ''),
(91, '2020-09-09', 16, 1, 500000, 52000, 552000, 0, ''),
(92, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(93, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(94, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(95, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(96, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(97, '2020-09-09', 16, 1, 500000, 0, 500000, 0, ''),
(98, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(99, '2020-09-09', 16, 1, 500000, 0, 500000, 0, ''),
(100, '2020-09-09', 16, 0, 0, 0, 0, 0, ''),
(101, '2020-09-09', 16, 1, 500000, 0, 500000, 0, ''),
(102, '2020-09-09', 16, 1, 500000, 0, 500000, 0, ''),
(103, '2020-09-09', 16, 1, 500000, 106000, 606000, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_post`
--

CREATE TABLE `tb_post` (
  `id_post` int(10) UNSIGNED NOT NULL,
  `judul_post` varchar(100) NOT NULL,
  `isi_artikel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_post`
--

INSERT INTO `tb_post` (`id_post`, `judul_post`, `isi_artikel`) VALUES
(2, 'Sed congue ultrices velit semper ullamcorper. Donec ut urna posuere, bibendum urna id, tincidunt urn', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac purus eu nisi volutpat interdum. Aliquam pulvinar, sapien fermentum scelerisque ullamcorper, diam urna condimentum orci, et tristique nunc est ac quam. Aenean malesuada sapien vitae porta bibendum. Vestibulum aliquet, mauris vitae consequat dapibus, arcu orci aliquet diam, eu tincidunt augue nisi at dui. Etiam ullamcorper nibh sed enim varius, nec ultricies sem interdum. Duis condimentum sit amet erat in consectetur. Etiam sem ante, vestibulum a eros ut, lacinia molestie lacus.\r\n\r\nVestibulum sit amet iaculis purus. Etiam tempus id mauris quis lobortis. Donec suscipit dui at erat lacinia, ac dignissim est fringilla. Vestibulum lacinia elit sit amet quam posuere, ac fringilla justo semper. Nulla vitae mauris massa. Sed scelerisque gravida nisl sit amet convallis. Ut nec convallis leo. Suspendisse velit ante, volutpat cursus erat quis, fringilla feugiat sapien. Donec viverra sem quis odio sollicitudin tempus. Praesent feugiat leo vel orci vestibulum dapibus. Phasellus pretium tristique odio id vulputate.\r\n\r\nPellentesque mi quam, varius nec pellentesque nec, rhoncus a enim. Etiam eu velit efficitur felis mollis rutrum. Nullam arcu metus, mattis vel venenatis sed, lobortis malesuada nunc. Nulla pellentesque turpis et erat ultricies egestas. Sed aliquet ultrices nibh non sodales. Aenean sed libero tristique, tristique urna eu, bibendum tortor. Fusce a convallis ante, sed malesuada leo.\r\n\r\nNam dapibus tincidunt quam. Ut ut mauris enim. Praesent auctor leo et suscipit faucibus. Ut in varius leo, nec bibendum quam. Suspendisse lectus velit, ultricies in justo vitae, dapibus gravida nulla. Praesent ullamcorper faucibus auctor. Ut dictum velit hendrerit, accumsan purus quis, ullamcorper quam.\r\n\r\nSuspendisse potenti. Duis euismod, nunc eu tempor tristique, metus neque ullamcorper quam, quis efficitur enim elit euismod sem. Ut sed porta lacus. Maecenas leo ipsum, interdum vel sem id, ornare efficitur dolor. Cras vel ultrices nulla. Nullam molestie, odio eu egestas dapibus, tellus neque sollicitudin risus, sed fermentum leo eros sit amet justo. Nullam a imperdiet justo.'),
(3, 'Pellentesque eget ex risus. Suspendisse lacinia facilisis dictum. Aliquam sed lacinia lorem, in vulp', 'Suspendisse quam nunc, finibus eget pulvinar in, tempus sit amet quam. Nulla sed pellentesque tortor. Sed interdum ultricies nibh sit amet tempus. Nullam sit amet vulputate metus. Duis ligula risus, fringilla ut posuere finibus, vehicula sed est. Nullam eu odio nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras ac ligula at est pharetra elementum. Aliquam erat volutpat. Duis quis turpis nec quam tristique interdum non eu mauris. Etiam id elit auctor, vestibulum lectus sit amet, viverra nibh.\r\n\r\nSuspendisse eget rhoncus orci. Morbi non elit a magna porttitor placerat sed eget nibh. Maecenas fermentum ante vel convallis posuere. Nulla nec lacus turpis. Proin condimentum facilisis dui hendrerit dapibus. Sed sem libero, bibendum at augue non, lacinia bibendum odio. In lobortis luctus tellus quis luctus. Duis a mi vestibulum, suscipit erat id, maximus lectus. In dignissim non quam ut iaculis. Praesent consequat leo id eros dignissim fermentum. Phasellus fringilla, eros congue pulvinar tincidunt, nibh justo tempus elit, ac rutrum quam nulla eu ex. Cras lobortis eu nulla a placerat. Donec consectetur nunc eget augue condimentum consequat.\r\n\r\nSuspendisse tincidunt lacinia tortor. Maecenas vel tortor ut nisl molestie consectetur. Integer dictum quam eu nulla semper elementum. Duis eu neque sagittis, lacinia quam ut, tristique risus. Donec gravida massa et est mattis tincidunt. Integer nec malesuada nisi, sit amet aliquam enim. Suspendisse potenti. Vivamus luctus varius ipsum sit amet posuere. Nullam auctor fermentum ipsum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur ut venenatis nulla. Mauris diam lacus, aliquam vel vestibulum sit amet, volutpat vel nibh. Mauris turpis risus, pharetra sit amet ipsum placerat, iaculis aliquet orci. Vestibulum non ex sit amet ex hendrerit suscipit. Aliquam eget aliquam est, ut ornare augue.\r\n\r\nDuis mattis molestie nulla et fermentum. Nullam varius facilisis elit. Nunc cursus nisl ligula, eget dictum lectus lacinia ut. Fusce at cursus lorem, tincidunt vulputate turpis. In facilisis, nisl quis aliquet ultricies, risus tortor lobortis nisl, vel consequat nunc arcu eu nunc. Aliquam erat volutpat. Phasellus nec luctus risus. Maecenas aliquet pretium purus interdum semper.\r\n\r\nNulla eget tincidunt massa, id vestibulum tortor. Phasellus placerat blandit tellus, ac ullamcorper est. Nullam pellentesque nisl nec sem elementum sollicitudin. Nullam auctor sodales lobortis. Nunc lacinia posuere mauris eu tempus. Aliquam feugiat fringilla elit, eget molestie dolor vulputate nec. Suspendisse nisi urna, volutpat quis malesuada blandit, ullamcorper ac ante. In sit amet eleifend tortor, ac lacinia nisl. Integer auctor rutrum suscipit.\r\n\r\nInterdum et malesuada fames ac ante ipsum primis in faucibus. Sed tempor sit amet arcu accumsan sollicitudin. Etiam sollicitudin tempus iaculis. Ut in tortor eu nunc mattis consectetur vitae quis lacus. Proin ullamcorper dui ipsum, et ullamcorper augue posuere ac. Praesent porta justo quis interdum cursus. Mauris ut tellus purus. Nam id justo in libero fringilla tempus.\r\n\r\nIn orci massa, imperdiet quis ultricies ullamcorper, pharetra id purus. Mauris id arcu nibh. Pellentesque sit amet ante velit. Curabitur at vulputate mauris. Maecenas commodo pulvinar est, in sagittis dolor pulvinar at. Nullam consectetur dui vel tellus luctus dapibus. Nunc ornare sem id mi semper, nec suscipit mi dignissim.\r\n\r\nVestibulum a ipsum rutrum, feugiat nisl cursus, lacinia urna. Nunc pulvinar erat a iaculis sodales. Nam sed mauris et arcu auctor egestas. Pellentesque non velit tortor. Aenean accumsan nibh pharetra efficitur ornare. Aliquam ut porta nulla. Phasellus faucibus quis metus vel dignissim. Sed a ligula lorem. Fusce maximus elit sed urna efficitur maximus. Nam commodo leo purus, a venenatis mi cursus dictum. Donec auctor tortor mauris, vel efficitur diam blandit vel. Aenean facilisis tempus porttitor. Nullam volutpat eleifend maximus. Ut ultricies nisi orci, sed aliquet nibh interdum sit amet. Pellentesque dignissim nisl nisi, quis congue quam ornare in.\r\n\r\nPraesent ut tortor sed nisi sodales tempus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum feugiat tortor, at sollicitudin metus varius id. Ut gravida turpis sodales, euismod nulla ut, ultricies justo. Nulla facilisi. Maecenas placerat dignissim fermentum. In egestas ligula quis magna congue mollis. Pellentesque tempus dolor dolor, a lacinia leo fermentum blandit. Aliquam molestie sapien nec dictum sodales. Vestibulum in tempus neque. Duis a purus varius, varius eros at, fringilla diam. Duis molestie odio quis quam dapibus, quis gravida libero condimentum. Nunc orci leo, condimentum id feugiat efficitur, faucibus ac ante. Mauris rhoncus enim erat, at egestas neque tempor sit amet. Vestibulum nunc elit, imperdiet a efficitur vel, cursus non libero.\r\n\r\nNulla pulvinar imperdiet augue, et molestie lacus finibus et. Cras lacus nisi, mattis eget diam sed, volutpat maximus felis. Praesent aliquet maximus cursus. Donec est ex, dictum eu neque ac, gravida pulvinar nisi. Sed non odio at lacus blandit feugiat nec et metus. Pellentesque pharetra dui sapien, in luctus est tristique ac. Donec rutrum velit eget auctor consectetur. Maecenas nec lacus eu erat bibendum pharetra nec a sem. Sed facilisis vulputate lectus, et scelerisque urna scelerisque et.\r\n\r\nNunc pulvinar eros vitae nisl vehicula, eget aliquam urna imperdiet. Nullam vitae vestibulum eros. Aliquam pretium consectetur mi id faucibus. Mauris molestie diam varius ultricies dapibus. Ut lobortis tincidunt massa vel pretium. Donec consequat accumsan turpis vitae ullamcorper. Quisque eget sem quis est blandit feugiat et sed lorem.'),
(4, 'Aliquam a sollicitudin nibh. Phasellus hendrerit fringilla mauris, sed pretium ante euismod vel. Eti', 'Nulla venenatis nibh justo, in dignissim justo fringilla quis. Donec purus magna, tempus interdum dui auctor, vehicula tempus elit. Curabitur sit amet dapibus enim. Aenean pharetra ac ipsum ut rhoncus. In porta convallis commodo. Suspendisse ultricies finibus erat, sed ultricies nulla efficitur sit amet. Fusce venenatis eget ante ac maximus. Donec eu arcu porta, mattis turpis eget, consequat sem. Nam dapibus tortor erat, ut pulvinar nibh aliquet commodo.\r\n\r\nEtiam vel lacus arcu. Curabitur imperdiet suscipit suscipit. Nulla congue ac mi tristique tincidunt. Suspendisse potenti. Mauris tortor felis, maximus vel nunc nec, condimentum porta sapien. Etiam sed malesuada erat. Integer malesuada condimentum molestie. Cras rutrum facilisis fringilla. Praesent eu fringilla elit. Donec porttitor leo nec sapien luctus, posuere viverra ex pellentesque. Fusce non dui rutrum libero placerat dapibus. Aliquam sollicitudin vehicula interdum. Vestibulum volutpat porta massa at efficitur. In velit elit, blandit nec nulla vel, commodo placerat ante.\r\n\r\nVestibulum scelerisque arcu eu ipsum auctor vestibulum. Nunc ultricies turpis nibh, sit amet semper libero dictum nec. Nullam suscipit velit est, in tempus ex faucibus non. Aliquam id nulla ac neque interdum fermentum. Maecenas tincidunt molestie dapibus. Nullam eu tortor non elit facilisis faucibus. Pellentesque consequat interdum ipsum non facilisis. Nam fringilla non lectus in venenatis. Suspendisse dignissim porta eros id venenatis. Aenean vitae lectus quis orci varius aliquet nec non odio. Fusce ligula urna, vehicula a tempor id, commodo non est. Pellentesque ac quam ac dui sodales suscipit. Duis elementum, lorem at aliquet lobortis, tellus ligula iaculis lectus, eu consectetur risus dui vel arcu. Nullam sit amet luctus elit. Donec et felis dolor.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mollis efficitur vulputate. Cras sodales dolor non ipsum porta, ac scelerisque ipsum volutpat. Pellentesque id magna fringilla, tincidunt dolor at, congue nisi. Maecenas mauris nulla, congue eu posuere et, vestibulum ac tellus. Duis mattis pulvinar lorem id maximus. Nam ac ligula quis orci egestas gravida. Quisque facilisis sed ipsum et dignissim. Nam ac dui cursus dui varius tempor. Aliquam eget risus fringilla, euismod enim non, iaculis lacus. Vivamus non scelerisque ex.\r\n\r\nEtiam scelerisque est et luctus tincidunt. Nulla euismod justo nisi, a ultricies urna eleifend et. Donec volutpat nunc sed ante imperdiet, at mollis metus porta. Aenean nec eros in eros consequat bibendum a ullamcorper risus. Praesent non feugiat quam. Quisque vitae neque fringilla, auctor erat imperdiet, feugiat nunc. Sed suscipit, nisi in posuere sodales, dui tortor sollicitudin justo, convallis tempor sapien est a mi. Cras maximus, ipsum vitae vestibulum malesuada, justo tortor gravida lacus, sit amet pharetra mauris erat quis leo. Duis vitae nisi tristique lectus tincidunt ultrices et eget arcu. Duis sed aliquet orci, non lobortis neque. Maecenas in nisi ut odio feugiat finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed imperdiet ligula sed odio finibus, a tincidunt mi porttitor. Duis imperdiet odio quis varius iaculis.\r\n\r\nIn hac habitasse platea dictumst. Maecenas viverra, sem non sagittis sodales, metus nibh vestibulum odio, vel dapibus nisl nulla et turpis. Aliquam tristique iaculis mauris, sit amet tristique augue posuere eu. Nullam vel dictum metus, ac facilisis diam. Praesent ut ipsum nec enim bibendum imperdiet vitae sed urna. Nam ligula lacus, facilisis non magna eget, molestie fermentum turpis. Proin molestie pharetra tempor. Donec placerat condimentum rutrum. Proin et erat eleifend, lacinia velit a, dapibus nunc. Aenean in enim leo. Proin ut tristique quam.\r\n\r\nVestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut pharetra urna in tellus scelerisque varius. Proin sit amet est vel odio tempus tristique sed et lectus. Morbi egestas ullamcorper orci quis semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi. Aenean augue turpis, gravida et purus vitae, lacinia lacinia eros. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed lorem mauris, faucibus eu tincidunt nec, fermentum vitae mauris. Suspendisse pharetra rhoncus ornare.\r\n\r\nNam volutpat viverra ante eget pellentesque. Sed malesuada massa tincidunt semper luctus. Integer mollis nisl nec augue aliquet, non pellentesque lorem laoreet. Donec dictum suscipit arcu sed luctus. Sed quis congue quam, vitae mattis neque. Ut vitae posuere tellus. Maecenas rhoncus mauris vitae volutpat tincidunt. Vestibulum sed finibus ipsum. Ut fermentum ultricies quam sed eleifend. Fusce luctus tempor dolor, ut vestibulum metus rutrum quis.\r\n\r\nDonec faucibus ex at ante mollis varius. Phasellus semper felis ac tristique facilisis. Integer non augue condimentum, mattis purus sed, pretium lacus. Morbi lobortis sollicitudin justo, vel pulvinar est rhoncus ac. Nunc placerat, ante sit amet efficitur porttitor, ipsum leo euismod diam, eu ultricies tortor lectus vel arcu. Suspendisse placerat ex vel mauris hendrerit vestibulum. In non erat vitae dui suscipit aliquam. Integer id turpis luctus, fringilla lectus nec, cursus arcu. Maecenas ac risus est. Ut eu leo convallis, molestie massa nec, mattis tellus. Phasellus ut tincidunt ante.');

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id_produk` int(10) UNSIGNED NOT NULL,
  `id_kategori` int(10) UNSIGNED NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `harga_jual` int(10) UNSIGNED NOT NULL,
  `harga_modal` int(10) UNSIGNED NOT NULL,
  `stok` int(10) UNSIGNED NOT NULL,
  `berat` int(10) UNSIGNED NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `id_kategori`, `nama_produk`, `gambar`, `harga_jual`, `harga_modal`, `stok`, `berat`, `deskripsi`) VALUES
(1, 6, 'Kaftan Pastel Ria Miranda', '0c0bfad35900d4d14fb164053b8ff741.jpg', 120000, 80000, 10, 200, '- Kaftan nuansa pastel dengan tali ikat \r\n- Warna ungu\r\n- Kerah bulat \r\n- Unlined\r\n- Loose fit\r\n- Kancing belakang\r\n- Detail ruffle dibagian lengan\r\n- Desain full motif\r\n- Material cerruti\r\n- Tinggi model 172cm, mengenakan ukuran one size'),
(2, 6, 'Gamis dress motif etnik detail cuff asimetris', 'kamilaa-by-itang-yunasz-1458-1040481-1', 120000, 80000, 10, 200, '- Gamis dress motif etnik detail cuff asimetris\r\n- Warna biru\r\n- Kerah bulat\r\n- Unlined\r\n- Regular fit\r\n- Kancing dan resleting belakang\r\n- Detail ruffle hemline\r\n- Material katun\r\n- Tinggi model 171cm, mengenakan ukuran S'),
(3, 6, 'Kaftan desain hem batwing', '4AAQSkZJRgABAQAAAQABAAD.jpg', 599000, 479200, 12, 200, '- Kaftan muslim desain hem batwing\r\n- Warna hijau\r\n- Kerah bulat\r\n- Unlined\r\n- Relaxed fit\r\n- Kancing belakang\r\n- Material chiffon\r\n- Tinggi model 172cm, menggunakan ukuran M'),
(4, 7, 'Hijab Lubna - Shafiya News II', 'c89b948dcef9fc8d30e78cba4ca88820.jpg', 269000, 150000, 12, 200, '- Solid-colored scarf with lace detail\r\n- Chiffon\r\n- Stitched squared hem\r\n- Dimensions: L184cm x W72cm\r\n- Model wears a size One Size and is 175cm tall'),
(5, 7, 'kamilaa-by-itang-yunasz', 'kamilaa-by-itang-yunasz-1458-1040481-1.jpg', 269000, 150000, 12, 200, '- Solid-colored scarf with lace detail\r\n- Chiffon\r\n- Stitched squared hem\r\n- Dimensions: L184cm x W72cm\r\n- Model wears a size One Size and is 175cm tall'),
(6, 7, 'Lace Edge Scarf 7921', 'lubna-7921-1101061-1.jpg', 269000, 150000, 12, 200, '- Solid-colored scarf with lace detail\r\n- Chiffon\r\n- Stitched squared hem\r\n- Dimensions: L184cm x W72cm\r\n- Model wears a size One Size and is 175cm tall'),
(7, 8, 'RA Hijab\nTazkia', 'ra-hijab-1338-5970091-1.jpg', 99000, 69300, 12, 200, '- Hijab scarf segi empat warna solid\r\n- Warna kuning\r\n- Material double hycon\r\n- Bagian pinggir dijahit\r\n- Panjang x Lebar: 110cm x 110cm'),
(26, 1, 'Diindri Hijab\nJilbab Instan Premium - Pink Lotus', 'diindri-hijab-1434-2554061-1.jpg', 120000, 80000, 12, 200, 'kaos oblong turn back crime'),
(27, 1, 'Buttonscarves\nSofya Square Voile Cotton In Oregano', 'buttonscarves-3284-5986602-1.jpg', 120000, 80000, 12, 200, 'kaos rock and roll'),
(28, 1, 'Sofya Square Voile Cotton In Pinksalt', 'buttonscarves-3286-8986602-1.jpg', 120000, 80000, 12, 200, 'kaos oblong the beatles'),
(29, 1, 'TwentySix Hijab\nAiko', 'twentysix-hijab-9369-7235851-1.jpg', 120000, 80000, 12, 200, 'kaos bola'),
(30, 1, 'RA Hijab\nSavana', 'ra-hijab-6933-1589002-1.jpg', 120000, 80000, 12, 200, 'kaos oblong turn back crime'),
(31, 1, 'RA Hijab\nPs. Paisley', 'ra-hijab-4408-5968602-1.jpg', 120000, 80000, 11, 200, 'kaos rock and roll'),
(32, 1, 'RA Hijab\nLilyan', 'ra-hijab-3732-2789002-1.jpg', 120000, 80000, 5, 200, 'kaos oblong the beatles'),
(34, 1, 'RA Hijab\nKarrisa Jmb', 'ra-hijab-3619-7780091-1.jpg', 120000, 80000, 6, 200, 'test'),
(35, 1, 'RA Hijab\nB.Adera Kom', 'ra-hijab-3154-9990091-1.jpg', 120000, 80000, 6, 200, 'test'),
(36, 8, 'Flowy Square Hijab', 'byoutcotton-8146-5478841-1.jpg', 99000, 69300, 7, 200, '- Hijab scarf segi empat warna solid\r\n- Warna kuning\r\n- Material double hycon\r\n- Bagian pinggir dijahit\r\n- Panjang x Lebar: 110cm x 110cm'),
(37, 6, 'Novella Scarves\nI Heart You Katie', 'novella-scarves-0235-3969102-1.jpg', 500000, 200000, 168, 300, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `tb_shopping_cart`
--

CREATE TABLE `tb_shopping_cart` (
  `id_shopping_cart` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_produk` int(10) UNSIGNED NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_shopping_cart`
--

INSERT INTO `tb_shopping_cart` (`id_shopping_cart`, `id_user`, `id_produk`, `jumlah`) VALUES
(11, 1, 37, 1),
(18, 16, 37, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` smallint(5) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `RW` varchar(5) NOT NULL,
  `RT` varchar(5) NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `email`, `password`, `nama_lengkap`, `alamat`, `telepon`, `id_provinsi`, `provinsi`, `id_kota`, `kota`, `kecamatan`, `kelurahan`, `RW`, `RT`, `kodepos`, `role`) VALUES
(1, 'admin', 'admin@email.com', '$2y$10$kcompgSft7eRgecgetnMEexRI/udrdwnSdq5mT8rrqFXwil41IpGK', 'nama admin', 'alamat admin', '1234', 0, '', 0, '', '', '', '', '', '12345', 1),
(2, '', 'lita@email.com', '$2y$10$gRhLBLno.m5Xi7OxPGlaf.MqRQlHjaBpDAKoTTVPzP9A1pqbY1lEC', 'yayan', 'alamat yayan', '522364', 1, 'Bali', 1, 'Nanggroe Aceh Darussalam (NAD)', '123', '44233123', '3', '2', '12356', 0),
(16, '', 'lita@gmail.com', '$2y$10$kcompgSft7eRgecgetnMEexRI/udrdwnSdq5mT8rrqFXwil41IpGK', 'Lita', 'test', '1234', 1, 'Bali', 1, 'Nanggroe Aceh Darussalam (NAD)', '1', '1', '1', '1', '1234', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_detail_penjualan`
--
ALTER TABLE `tb_detail_penjualan`
  ADD PRIMARY KEY (`id_detail_penjualan`);

--
-- Indexes for table `tb_dipesan`
--
ALTER TABLE `tb_dipesan`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- Indexes for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
  ADD PRIMARY KEY (`id_gambar`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_komentar`
--
ALTER TABLE `tb_komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tb_pengiriman`
--
ALTER TABLE `tb_pengiriman`
  ADD PRIMARY KEY (`id_pengiriman`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `tb_post`
--
ALTER TABLE `tb_post`
  ADD PRIMARY KEY (`id_post`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tb_shopping_cart`
--
ALTER TABLE `tb_shopping_cart`
  ADD PRIMARY KEY (`id_shopping_cart`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_detail_penjualan`
--
ALTER TABLE `tb_detail_penjualan`
  MODIFY `id_detail_penjualan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `tb_dipesan`
--
ALTER TABLE `tb_dipesan`
  MODIFY `id_pesanan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
  MODIFY `id_gambar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_komentar`
--
ALTER TABLE `tb_komentar`
  MODIFY `id_komentar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `tb_pengiriman`
--
ALTER TABLE `tb_pengiriman`
  MODIFY `id_pengiriman` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  MODIFY `id_penjualan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `tb_post`
--
ALTER TABLE `tb_post`
  MODIFY `id_post` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id_produk` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_shopping_cart`
--
ALTER TABLE `tb_shopping_cart`
  MODIFY `id_shopping_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
