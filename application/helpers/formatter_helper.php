<?php
function number_thousand_delimiter_format($number){
	return number_format($number,0,",",".");
}

function mysql_to_dmy_format($date){
	return date("d-m-Y", strtotime($date));
}
?>