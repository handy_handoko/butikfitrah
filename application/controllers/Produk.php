<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index() {
		$this->load->model('produk_model');
		$this->load->view('produk/tabel', array(
			'data'=>$this->produk_model->get_all(array('tb_produk.id_produk', 'nama_produk', 'harga_jual', 'stok'))
		));
	}

	public function add() {
		$this->load->helper('form_helper');
		$this->load->model('produk_model');
		$this->load->model('kategori_model');
		$this->load->view('produk/form', array(
			'kategori'=>$this->kategori_model->get_data(),
		));
	}

	public function edit($id) {
		$this->load->helper('form_helper');
		$this->load->model('produk_model');
		$this->load->model('kategori_model');
		$this->load->view('produk/form', array(
			'kategori'=>$this->kategori_model->get_data(),
			'data'=>$this->produk_model->get(
				array('tb_produk.id_produk', 'nama_produk', 'id_kategori', 'harga_jual', 'harga_modal', 'stok', 'berat', 'deskripsi'),
				$id
			)
		));
	}

	public function delete($id) {
		$this->load->model('produk_model');
		$this->produk_model->delete($id);
		redirect('produk');
	}

	public function save() {
		$this->load->model(array('produk_model', 'image_model'));
		if($this->input->post("id_produk")!= NULL){
			$id_produk = $this->input->post("id_produk");
			$this->produk_model->update($this->input->post());
		} else
			$id_produk = $this->produk_model->add($this->input->post());


		if (!empty($_FILES['gambar']['name'][0])) {
			// print_r($_FILES['gambar']['name']);
			$config['upload_path'] = './upload/product/';
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size'] = 2048;
			$config['max_width'] = 2048;
			$config['max_height'] = 2048;
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			for($i = 0; $i < count($_FILES['gambar']['name']); $i++){
                $_FILES['gambar']['name']		= $_FILES['gambar']['name'][$i];
                $_FILES['gambar']['type']		= $_FILES['gambar']['type'][$i];
                $_FILES['gambar']['tmp_name']	= $_FILES['gambar']['tmp_name'][$i];
                $_FILES['gambar']['error']		= $_FILES['gambar']['error'][$i];
                $_FILES['gambar']['size']		= $_FILES['gambar']['size'][$i];

				if ( ! $this->upload->do_upload('gambar')) {
					// print_r($this->upload->display_errors());
					return false;
				} else {
					if($i==0)
						$this->produk_model->update(array(
							'id_produk'=>$id_produk,
							'gambar'=>$this->upload->data("file_name")
						));
					$data['id_produk'] = $id_produk;
					$data['nama_gambar'] = $this->upload->data("file_name");
					$data['main_image'] = ($i==0?1:0);
					if($this->input->post("id_produk")!= NULL)
						$this->image_model->delete($this->input->post("id_produk"));
					$this->image_model->insert($data);
				}
			}
		}
		redirect("produk");
	}
}