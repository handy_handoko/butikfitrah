<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function browse() {
		$this->load->model('User_model');
		$this->load->view('user/tabel', array(
			'data'=>$this->User_model->get_all(
				array('id_user', 'username', 'email'),
				array('role'=>0)
			)
		));
	}

	public function add() {
		$this->load->helper('form_helper');
		$this->load->view('user/form');
	}

	public function save() {
		$this->load->model('user_model');
		$this->user_model->add($this->input->post());
		redirect("user/browse");
	}

	private function get_data($url){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_FOLLOWLOCATION => TRUE,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"key: be6d23888be8e0ee82cef8811ce3ce69"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		return null;
		} else {
			$response = json_decode($response);
			return $response;
		}
	}

	public function index() {
		$this->load->helper('form_helper');
		
		$data_provinsi = array();
		$response = $this->get_data("https://api.rajaongkir.com/starter/province");
		foreach ($response->rajaongkir->results as $provinsi) {
			$data_provinsi[$provinsi->province_id] = $provinsi->province;
		}

		$data_kota = array();
		$response = $this->get_data("https://api.rajaongkir.com/starter/city");
		foreach ($response->rajaongkir->results as $kota) {
			$data_kota[$kota->city_id] = $kota->city_name;
		}

		$this->load->model(array('kategori_model'));
		$kategori = $this->kategori_model->get_kategori();

		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('user/login', array(
			'provinsi' => $data_provinsi,
			'kota' => $data_kota,
		));
		$this->load->view('footer/user');
	}

	public function register() {
		$this->load->model('user_model');
		$this->user_model->register($this->input->post());
		$user = $this->user_model->login($this->input->post());
		if($user){
			$this->session->set_userdata($user);
			redirect("transaksi");
		}
	}

	public function edit_profile() {
		$this->load->model(array('produk_model', 'kategori_model'));
		$kategori = $this->kategori_model->get_kategori();
		
		$data_provinsi = array();
		$response = $this->get_data("https://api.rajaongkir.com/starter/province");
		foreach ($response->rajaongkir->results as $provinsi) {
			$data_provinsi[$provinsi->province_id] = $provinsi->province;
		}

		$data_kota = array();
		$response = $this->get_data("https://api.rajaongkir.com/starter/city");
		foreach ($response->rajaongkir->results as $kota) {
			$data_kota[$kota->city_id] = $kota->city_name;
		}

		$this->load->model('user_model');
		$this->load->helper('form_helper');
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('user/edit_profile', array(
			'profile'=>$this->user_model->get(
				array('email', 'username', 'nama_lengkap', 'alamat', 'telepon', 'id_provinsi', 'id_kota', 'kecamatan', 'kelurahan', 'RW', 'RT', 'kodepos',),
				$this->session->userdata('id_user')
			),
			'provinsi' => $data_provinsi,
			'kota' => $data_kota,
		));
		$this->load->view('footer/user');
	}

	public function update() {
		$this->load->model('user_model');
		$this->user_model->update($this->input->post());
	}

	public function login() {
		$this->load->model('user_model');
		$user = $this->user_model->login($this->input->post());
		if($user){
			$this->session->set_flashdata('success', 'login success');
			$this->session->set_userdata($user);
			redirect("transaksi");
		} else{
			$this->session->set_flashdata('error', 'login gagal');
			redirect('user/');
		}
	}

	public function logout() {
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect('/user');
	}
}