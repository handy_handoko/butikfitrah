<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index() {
		redirect('report/penjualan');
	}

	public function penjualan() {
		$this->load->model('penjualan_model');
		$this->load->helper('form_helper');
		$dateFilter = array();
		$date = array();
		if($this->input->post()){
			$dateFilter['tanggal_penjualan >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date'))));
			$dateFilter['tanggal_penjualan <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date'))));
			$date = $this->input->post();
		}

		$this->load->view('report/penjualan', array(
			'data'=>$this->penjualan_model->get_all_detail(
				array(' DATE_FORMAT(tanggal_penjualan, "%d-%m-%Y") as tanggal_penjualan', 'nama_produk', 'jumlah', 'harga', 'subtotal', 'tb_detail_penjualan.harga_modal'),
				$dateFilter
			),
			'date'=>$date
		));
	}

	public function rekap_penjualan() {
		$this->load->model('penjualan_model');
		$this->load->helper('form_helper');
		$dateFilter = array();
		$date = array();
		if($this->input->post()){
			$dateFilter['tanggal_penjualan >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date'))));
			$dateFilter['tanggal_penjualan <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date'))));
			$date = $this->input->post();
		}

		$this->load->view('report/rekap_penjualan', array(
			'data'=>$this->penjualan_model->get_all_summary(
				array(' DATE_FORMAT(tanggal_penjualan, "%d-%m-%Y") as tanggal_penjualan', 'nama_produk', 'SUM(`total_harga`) AS `total_penjualan`'),
				$dateFilter,
				array('tanggal_penjualan')
			),
			'date'=>$date
		));
	}

	public function pdfpenjualan() {
		$this->load->model('penjualan_model');
		$this->load->helper(array('form_helper', 'dompdf_helper'));
		$dateFilter = array();
		$date = array();
		if($this->input->get()){
			$dateFilter['tanggal_penjualan >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('start'))));
			$dateFilter['tanggal_penjualan <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('end'))));
		}

		$html = $this->load->view('report/pdf_penjualan', array(
			'data'=>$this->penjualan_model->get_all_detail_customer(
				array(' DATE_FORMAT(tanggal_penjualan, "%d-%m-%Y") as tanggal_penjualan', 'nama_produk', 'jumlah', 'harga', 'subtotal', 'tb_detail_penjualan.harga_modal', 'alamat', 'telepon'),
				$dateFilter
			)
		), true);
		pdf_create($html, 'laporan peminjaman');
	}

	public function reportpenjualan() {
		$this->load->model('penjualan_model');
		$this->load->helper('form_helper');
		$dateFilter = array();
		$date = array();
		if($this->input->get()){
			$dateFilter['tanggal_penjualan >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('start'))));
			$dateFilter['tanggal_penjualan <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('end'))));
		}

		$this->load->view('report/report_penjualan', array(
			'data'=>$this->penjualan_model->get_all_detail_customer(
				array(' DATE_FORMAT(tanggal_penjualan, "%d-%m-%Y") as tanggal_penjualan', 'nama_produk', 'jumlah', 'harga', 'subtotal', 'tb_detail_penjualan.harga_modal', 'alamat', 'telepon'),
				$dateFilter
			)
		));
	}

	public function peminjaman() {
		$this->load->model('peminjaman_model');
		$this->load->helper('form_helper');
		$dateFilter = array();
		$date = array();
		if($this->input->post()){
			$dateFilter['tanggal_peminjaman >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date'))));
			$dateFilter['tanggal_peminjaman <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date'))));
			$date = $this->input->post();
		}

		$this->load->view('report/peminjaman', array(
			'data'=>$this->peminjaman_model->get_all_detail(
				array(' DATE_FORMAT(tanggal_peminjaman, "%d-%m-%Y") as tanggal_peminjaman', 'nama_peminjam', 'nama_produk', 'jumlah', 'harga_sewa', 'denda'),
				$dateFilter
			),
			'date'=>$date
		));
	}

	public function pdfpeminjaman() {
		$this->load->model('peminjaman_model');
		$this->load->helper(array('form_helper', 'dompdf_helper'));
		$dateFilter = array();
		$date = array();
		if($this->input->get()){
			$dateFilter['tanggal_peminjaman >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('start'))));
			$dateFilter['tanggal_peminjaman <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('end'))));
		}

		$html = $this->load->view('report/pdf_peminjaman', array(
			'data'=>$this->peminjaman_model->get_all_detail(
				array(' DATE_FORMAT(tanggal_peminjaman, "%d-%m-%Y") as tanggal_peminjaman', 'nama_peminjam', 'nama_produk', 'jumlah', 'harga_sewa', 'denda'),
				$dateFilter
			)
		), true);
		pdf_create($html, 'laporan peminjaman');
	}

	public function reportpeminjaman() {
		$this->load->model('peminjaman_model');
		$this->load->helper('form_helper');
		$dateFilter = array();
		$date = array();
		if($this->input->get()){
			$dateFilter['tanggal_peminjaman >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('start'))));
			$dateFilter['tanggal_peminjaman <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->get('end'))));
		}

		$this->load->view('report/report_peminjaman', array(
			'data'=>$this->peminjaman_model->get_all_detail(
				array(' DATE_FORMAT(tanggal_peminjaman, "%d-%m-%Y") as tanggal_peminjaman', 'nama_peminjam', 'nama_produk', 'jumlah', 'harga_sewa', 'denda'),
				$dateFilter
			)
		));
	}
}