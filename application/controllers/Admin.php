<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function browse() {
		$this->load->model('User_model');
		$this->load->view('admin/tabel', array(
			'data'=>$this->User_model->get_all(
				array('id_user', 'username', 'email'),
				array('role'=>1)
			)
		));
	}

	public function index() {
		$this->load->helper('form_helper');
		$this->load->view('admin/login');
	}
	
	public function login() {
		$this->load->model('user_model');
		$user = $this->user_model->login($this->input->post());
		if($user){
			$this->session->set_userdata($user);
			redirect("produk");
		}
			redirect("admin");
	}

	public function logout() {
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect('/admin');
	}

	public function dashboard(){
		$this->load->helper('form_helper');
		$dateFilter = array();
		$date = array();
		if($this->input->post()){
			$dateFilter['tanggal_penjualan >='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date'))));
			$dateFilter['tanggal_penjualan <='] = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date'))));
			$date = $this->input->post();
		}

		$this->load->model('penjualan_model');
		$total_penjualan = $this->penjualan_model->get_all(
			'count(id_penjualan) as total_penjualan',
			'MONTH(`tanggal_penjualan`) = MONTH(CURRENT_DATE())
			AND YEAR(`tanggal_penjualan`) = YEAR(CURRENT_DATE())'
		);

		$this->load->model('produk_model');
		$total_produk = $this->produk_model->get_all_produk(
			'count(id_produk) as jumlah_barang',
			array()
		);

		$this->load->model('produk_model');
		$total_produk_habis = $this->produk_model->get_all_produk(
			'count(id_produk) as jumlah_barang',
			array('stok' => 0)
		);

		$this->load->view('admin/dashboard', array(
			'total_penjualan'=>$total_penjualan[0]['total_penjualan'],
			'total_produk'=>$total_produk[0]['jumlah_barang'],
			'total_produk_habis'=>$total_produk_habis[0]['jumlah_barang'],
			'data_harian'=>$this->penjualan_model->get_all_summary(
				array(' DATE_FORMAT(tanggal_penjualan, "%d-%m-%Y") as tanggal_penjualan', 'nama_produk', 'SUM(`total_harga`) AS `total_penjualan`'),
				$dateFilter,
				array('tanggal_penjualan')
			),
			'data_mingguan'=>$this->penjualan_model->get_all_summary(
				array(' DATE_FORMAT(tanggal_penjualan, "%d-%m-%Y") as tanggal_penjualan', 'nama_produk', 'SUM(`total_harga`) AS `total_penjualan`'),
				$dateFilter,
				'WEEK(`tanggal_penjualan`)'
			),
			'date'=>$date,
		));
	}
}
