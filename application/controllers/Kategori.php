<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index() {
		$this->load->model('kategori_model');
		$this->load->view('kategori/tabel', array(
			'data'=>$this->kategori_model->get_all(array('id_kategori', 'nama_kategori', 'jenis_kategori'), array())
			));
	}

	public function add() {
		$this->load->helper('form_helper');
		$this->load->view('kategori/form');
	}

	public function edit($id) {
		$this->load->helper('form_helper');
		$this->load->model('kategori_model');
		$this->load->view('kategori/form', array(
			'data'=>$this->kategori_model->get(
				array('id_kategori', 'nama_kategori', 'jenis_kategori'),
				$id
			)
		));
	}

	public function delete($id) {
		$this->load->model('kategori_model');
		$this->kategori_model->delete($id);
		redirect('produk');
	}

	public function save() {
		$this->load->model(array('kategori_model'));
		if($this->input->post("id_kategori")!= NULL){
			$this->kategori_model->update($this->input->post());
		} else
			$this->kategori_model->add($this->input->post());

		redirect("kategori");
	}
}