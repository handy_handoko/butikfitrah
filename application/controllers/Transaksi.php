<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->helper('form_helper');
	}

	private function is_auth(){
		if(!$this->session->userdata('id_user')){
			redirect('user');
		}
	}

	public function dashboard(){
		$this->load->view('dashboard');
	}
	
	public function index(){
		$this->load->model(array('produk_model', 'kategori_model', 'Post_model'));
		$kategori = $this->kategori_model->get_kategori();
		$produk = $this->produk_model->get_all_terbaru(array('id_produk', 'nama_produk', 'gambar', 'harga_jual'));
		$post = $this->Post_model->get_all(
			array(),
			array()
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/home', array(
			'produk'=>$produk,
			'post'=>$post,
		));
		$this->load->view('footer/user');
	}

	public function kategori($id_kategori) {
		$this->load->model(array('produk_model', 'kategori_model'));
		$kategori = $this->kategori_model->get_kategori();
		$produk = $this->produk_model->get_all_produk(
			array(),
			array('id_kategori'=>$id_kategori)
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/kategori', array('produk'=>$produk));
		$this->load->view('footer/user');
	}

	public function addToCart($id_produk){
		$this->is_auth();
		$this->load->model(array('cart_model', 'produk_model'));
		$produk = $this->produk_model->get('harga_jual, stok', $id_produk);
		if($produk['stok']>0){
			$data = array(
				"id_produk" => $id_produk,
			);

			$this->cart_model->insert($data);
		}
		redirect('transaksi/cart');
	}

	public function cart(){
		$this->is_auth();
		$this->load->helper('form_helper');
		$this->load->model(array('cart_model', 'kategori_model'));
		$kategori = $this->kategori_model->get_kategori();
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/cart.php',array(
			'data'=>$this->cart_model->get_all_cart(array('id_shopping_cart', 'tb_produk.id_produk', 'nama_produk', 'harga_jual', 'gambar', 'jumlah', 'stok', 'tb_produk.deskripsi'))
		));
		$this->load->view('footer/user');
	}

	public function detail($id) {
		$this->load->model(array('produk_model', 'kategori_model'));
		$kategori = $this->kategori_model->get_kategori();
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/detail', array(
			'data'=>$this->produk_model->get(
				array('tb_produk.id_produk', 'nama_produk', 'gambar', 'harga_jual', 'harga_modal', 'stok', 'berat', 'deskripsi'),
				$id
				)
			));
		$this->load->view('footer/user');
	}

	public function deleteFromCart($id){
		$this->is_auth();
		$this->load->model('cart_model');
		$this->cart_model->delete($id);
		redirect('transaksi/cart');
	}

	public function updateCart(){
		$this->is_auth();
		$this->load->model('cart_model');
		foreach ($this->input->post('id_shopping_cart') as $key => $value) {
			$data['id_shopping_cart']= $this->input->post('id_shopping_cart')[$key];
			$data['jumlah']= $this->input->post('jumlah')[$key];
			$this->cart_model->update($data);
		}
		redirect('transaksi/cart');
	}

	private function get_data($url){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_FOLLOWLOCATION => TRUE,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"key: be6d23888be8e0ee82cef8811ce3ce69"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		return null;
		} else {
			$response = json_decode($response);
			return $response;
		}
	}

	public function shipping(){
		$this->is_auth();
		$this->load->model(array('user_model', 'kategori_model'));
		$kategori = $this->kategori_model->get_kategori();
		
		$data_provinsi = array();
		$response = $this->get_data("https://api.rajaongkir.com/starter/province");
		foreach ($response->rajaongkir->results as $provinsi) {
			$data_provinsi[$provinsi->province_id] = $provinsi->province;
		}

		$data_kota = array();
		$response = $this->get_data("https://api.rajaongkir.com/starter/city");
		foreach ($response->rajaongkir->results as $kota) {
			$data_kota[$kota->city_id] = $kota->city_name;
		}

		$this->load->helper('form_helper');
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/pengiriman', array(
			'recipient'=>$this->user_model->get(
				array('email', 'username', 'nama_lengkap AS nama_penerima', 'alamat', 'telepon', 'id_provinsi', 'id_kota', 'kecamatan', 'kelurahan', 'RW', 'RT', 'kodepos',),
				$this->session->userdata('id_user')
			),
			'provinsi' => $data_provinsi,
			'kota' => $data_kota,
		));
	}

	public function process_shipping(){
		$this->is_auth();
		$this->load->model('pengiriman_model'); 
		$id_pengiriman = $this->pengiriman_model->insert($this->input->post());
		redirect('transaksi/checkOut/'.$id_pengiriman);
	}

	public function checkOut($id_pengiriman){
		$this->is_auth();
		$this->load->model(array('penjualan_model', 'cart_model', 'kategori_model')); 
		$kategori = $this->kategori_model->get_kategori();
		$rekap_penjualan = $this->penjualan_model->add($id_pengiriman);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/checkout.php', $rekap_penjualan);
		$this->load->view('footer/user');
	}

	public function confirm(){
		$this->is_auth();
		$this->load->helper('formatter_helper');
		$this->load->model(array('penjualan_model', 'kategori_model')); 
		$kategori = $this->kategori_model->get_kategori();
		$invoices = $this->penjualan_model->get_all_payment(
			array('id_pembayaran','jumlah_dibayar', 'tanggal_berakhir'),
			array(
				'tb_pembayaran.customer_id'=>$this->session->userdata('id_user'), 
				'status_pembayaran'=>0
			)
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/confirm.php', array('invoices'=>$invoices));
		$this->load->view('footer/user.php');
	}

	public function detail_payment($id_pembayaran){
		$this->is_auth();
		$this->load->helper('form_helper');
		$this->load->model(array('penjualan_model', 'kategori_model')); 
		$kategori = $this->kategori_model->get_kategori();
		$invoices = $this->penjualan_model->get_all_payment(
			array('id_pembayaran', 'tb_pembayaran.id_penjualan', 'DATE_FORMAT(tanggal_penjualan,"%d-%m-%Y") as tanggal_penjualan', 'jumlah_dibayar', 'ongkos_kirim'),
			array(
				'id_pembayaran'=>$id_pembayaran, 
				'status_pembayaran'=>0
			)
		)[0];

		$invoices['order_detail'] = $this->penjualan_model->get_all_detail_transaction(
			array('nama_produk', 'jumlah', 'subtotal', 'harga'),
			array('tb_penjualan.id_penjualan'=>$invoices['id_penjualan'])
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/detail_confirm.php', $invoices);
		$this->load->view('footer/user.php');
	}

	public function process_confirm(){
		$this->is_auth();
		$this->load->helper('form_helper');
		$this->load->model(array('penjualan_model'));
		$this->penjualan_model->confirm_payment($this->input->post());
		$this->session->set_flashdata('success', 'Konfirmasi pembayaran berhasil.');
		redirect('transaksi/confirm/');
	}

	public function browse_payment(){
		$this->is_auth();
		$this->load->model(array('penjualan_model'));
		$payment = $this->penjualan_model->get_all_customer_payment(
			array('id_pembayaran', 'nama_lengkap', 'jumlah_dibayar as tagihan', 'nama_pengirim', 'DATE_FORMAT(tanggal_berakhir,"%d-%m-%Y") as tanggal_berakhir', 'bank', 'no_rekening', 'nama_pengirim', 'tanggal', 'jumlah', 'status_pembayaran'),
			array('status_pembayaran'=>1)
		);
		$this->load->view('transaksi/verify.php', array('payment'=>$payment));
	}

	public function browse_transaction(){
		$this->is_auth();
		$this->load->model(array('penjualan_model'));
		$transaksi = $this->penjualan_model->get_all_transaction(
			array('tb_penjualan.id_penjualan', 'DATE_FORMAT(tanggal_penjualan ,"%d-%m-%Y") as tanggal_penjualan', 'nama_lengkap', 'total', 'alamat', 'telepon', 'status'),
			array('status_pembayaran'=>2)
		);
		$sent = $this->penjualan_model->get_all_transaction(
			array('tb_penjualan.id_penjualan', 'DATE_FORMAT(tanggal_penjualan ,"%d-%m-%Y") as tanggal_penjualan', 'nama_lengkap', 'total', 'alamat', 'telepon', 'status'),
			array('status'=>2)
		);
		$this->load->view('transaksi/penjualan.php', array(
			'transaksi'=>$transaksi,
			'sent'=>$sent
		));
	}

	public function detail_transaction($id_transaksi){
		$this->is_auth();
		$this->load->helper('form_helper');
		$this->load->model(array('penjualan_model'));
		$detail_transaksi = $this->penjualan_model->get_all_detail_transaction(
			array('total_quantity', 'total_harga', 'ongkos_kirim', 'DATE_FORMAT(tanggal_penjualan,"%d-%m-%Y") as tanggal_penjualan', 'total', 'nama_lengkap', 'alamat', 'telepon', 'kodepos', 'nama_produk', 'jumlah'),
			array('tb_penjualan.id_penjualan'=>$id_transaksi)
		);
		$this->load->view('transaksi/detail_penjualan.php', array(
			'detail_transaksi'=>$detail_transaksi,
			'id_penjualan'=>$id_transaksi
		));
	}

	public function process_transaction(){
		$this->is_auth();
		$this->load->model(array('penjualan_model'));
		$respon = $this->penjualan_model->process_transaction($this->input->post());
		$pesan = "Pesanan anda telah dikirimkan dengan nomor resi ".$respon['resi'].".";
		// $this->send_mail($respon['email'], $pesan);
		redirect('transaksi/browse_transaction');
	}

	public function verify_payment($id_pembayaran){
		$this->is_auth();
		$this->load->model(array('penjualan_model'));
		$this->penjualan_model->verify_payment($id_pembayaran);
		$email = $this->penjualan_model->get_customer_payment(array('email'), $id_pembayaran);
		$pesan = "Pembayaran anda telah diterima. Pesanan anda akan segera diproses dan dikirim.";
		// $this->send_mail($email, $pesan);
		redirect('transaksi/browse_payment');
	}

	public function delete($id_pembayaran){
		$this->is_auth();
		$this->load->model(array('penjualan_model'));
		$this->penjualan_model->verify_payment($id_pembayaran);
		$email = $this->penjualan_model->get_customer_payment(array('email'), $id_pembayaran);
		$pesan = "Pembayaran anda telah diterima. Pesanan anda akan segera diproses dan dikirim.";
		// $this->send_mail($email, $pesan);
		redirect('transaksi/browse_payment');
	}

	public function clear_unpaid(){
		//kirim sudah expired
		$this->load->model('penjualan_model');
		$today_expired = $this->penjualan_model->clear_unpaid();
		$pesan = "Batas Pembayaran anda sudah expired. Silahkan melakukan pemesanan ulang";
		foreach ($today_expired as $expired) {
			$this->send_mail($expired['email'], $pesan);
		}
	}

	public function process(){
		$this->is_auth();
		$this->load->model(array('penjualan_model', 'kategori_model')); 
		$kategori = $this->kategori_model->get_kategori();
		$orders = $this->penjualan_model->get_all_payment(
			array('tb_penjualan.id_penjualan', 'tanggal_penjualan', 'total', 'status_pembayaran'),
			array(
				"tb_penjualan.customer_id" => $this->session->userdata('id_user'),
				"status"=>0
			)
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/proses.php', array('orders'=>$orders));
		$this->load->view('footer/user.php');
	}

	public function invoicedetail($id_penjualan){
		$this->is_auth();
		$this->load->helper('form_helper');
		$this->load->model(array('penjualan_model', 'kategori_model')); 
		$kategori = $this->kategori_model->get_kategori();
		$invoices = $this->penjualan_model->get_all_payment(
			array('id_pembayaran', 'tb_pembayaran.id_penjualan', 'DATE_FORMAT(tanggal_penjualan,"%d-%m-%Y") as tanggal_penjualan', 'jumlah_dibayar', 'ongkos_kirim'),
			array(
				'tb_penjualan.id_penjualan'=>$id_penjualan, 
			)
		)[0];

		$invoices['order_detail'] = $this->penjualan_model->get_all_detail_transaction(
			array('nama_produk', 'jumlah', 'subtotal', 'harga'),
			array('tb_penjualan.id_penjualan'=>$id_penjualan)
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/detail_penjualan_customer.php', $invoices);
		$this->load->view('footer/user.php');
	}

	public function sent(){
		$this->is_auth();
		$this->load->model(array('penjualan_model', 'kategori_model')); 
		$kategori = $this->kategori_model->get_kategori();
		$orders = $this->penjualan_model->get_all(
			array('id_penjualan', 'tanggal_penjualan', 'total', 'resi'),
			array(
				"customer_id" => $this->session->userdata('id_user'),
				"status"=>1
			)
		);
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('transaksi/sent.php', array('orders'=>$orders));
		$this->load->view('footer/user.php');
	}

	public function confirmsent($id_penjualan){
		$this->is_auth();
		$this->load->model('penjualan_model');
		$this->penjualan_model->update(array(
			'id_penjualan'	=> $id_penjualan, 
			'status'		=> 2
		));
		redirect('transaksi/sent');
	}

	public function send_sms($no_hp, $pesan){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://reguler.zenziva.net/apps/smsapi.php?userkey=ae9owi&passkey=tugasakhir&nohp=".$no_hp."&pesan=".urlencode($pesan));
		$data = curl_exec($ch);
		// print_r($data);
		curl_close($ch);
	}

	public function send_mail($email, $pesan){
		//Load email library
		$this->load->library('email');
		$this->load->library('encrypt');

		//SMTP & mail configuration
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'lita@stimednp.com',
			'smtp_pass' => 'lita',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		// $htmlContent = '<h1>Sending email via SMTP server</h1>';
		// $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';
		$htmlContent = $pesan;

		$this->email->to($email);
		$this->email->from('lita@stimed.com','produk');
		$this->email->subject('Butik Fitrah');
		$this->email->message($htmlContent);

		//Send email
		// $this->email->send();
		if(!$this->email->send()){
			print_r($this->email->print_debugger());
		}
	}

	public function getNotif(){
		if(! $this->input->is_ajax_request())
			redirect('404');

		$this->load->model(array(
			'penjualan_model',
		));
		$payment = $this->penjualan_model->get_all_customer_payment(
			array('COUNT(id_pembayaran) AS jumlah_pembayaran' ),
			array('status_pembayaran'=>1)
		);
		$transaction = $this->penjualan_model->get_all_transaction(
			array('COUNT(id_pembayaran) AS jumlah_transaksi' ),
			array('status_pembayaran'=>2, 'status'=>0)
		);

		header('Content-Type: application/json');
		echo json_encode(array(
			'payment' => $payment[0]['jumlah_pembayaran'],
			'transaction' => $transaction[0]['jumlah_transaksi']
		));
	}

	// public function testransaksi(){
	// 	$this->load->model('penjualan_model');
	// 	print_r($this->penjualan_model->get_ongkir(2));
	// }
}