<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index() {
		$this->load->model('post_model');
		$this->load->view('post/tabel', array(
			'data'=>$this->post_model->get_all(array('tb_post.id_post', 'judul_post'))
		));
	}

	public function add() {
		$this->load->helper('form_helper');
		$this->load->view('post/form');
	}

	public function edit($id) {
		$this->load->helper('form_helper');
		$this->load->model('post_model');
		$this->load->view('post/form', array(
			'data'=>$this->post_model->get(
				array('tb_post.id_post', 'judul_post', 'isi_artikel'),
				$id
			)
		));
	}

	public function view($id) {
		$this->load->helper('form_helper');
		$this->load->model('kategori_model');
		$kategori = $this->kategori_model->get_kategori();
		$this->load->model('post_model');
		$this->load->view('header/user', array('kategori'=>$kategori));
		$this->load->view('post/view', array(
			'post'=>$this->post_model->get(
				array('tb_post.id_post', 'judul_post', 'isi_artikel'),
				$id
			)
		));
		$this->load->view('footer/user');
	}

	public function delete($id) {
		$this->load->model('post_model');
		$this->post_model->delete($id);
		redirect('post');
	}

	public function save() {
		$this->load->model('post_model');
		if($this->input->post("id_post")!= NULL){
			$id_post = $this->input->post("id_post");
			$this->post_model->update($this->input->post());
		} else
			$id_post = $this->post_model->add($this->input->post());
		redirect("post");
	}
}