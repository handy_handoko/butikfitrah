<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function update($data = array()){
		$this->db->set("status", $data['status']);
		$this->db->where("id_penjualan",$data["id_penjualan"]);
		$this->db->update("tb_penjualan");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	function add($id_pengiriman){
		//buat data penjualan baru untuk mendapat id penjualan. id penjualan dipakai di detail penjualan
		$this->db->set("tanggal_penjualan", 'now()', false);
		$this->db->set("customer_id", $this->session->userdata('id_user'));
		$this->db->set("total_quantity", 0);
		$this->db->set("total_harga", 0);
		$this->db->insert("tb_penjualan");
		$id_penjualan = $this->db->insert_id();

		//get id produk, jumlah, dan harga jual dari tb_produk, tb_shopping_cart, yang jenis transaksi = 0 (transaksi penjualan)
		$cart = $this->db
				->select('tb_produk.id_produk, nama_produk, jumlah, harga_modal, harga_jual, tb_produk.berat')
				->join('tb_produk', 'tb_shopping_cart.id_produk = tb_produk.id_produk')
				->where(array(
					'id_user'=>$this->session->userdata('id_user')
					))
				->get("tb_shopping_cart")
				->result_array();
		
		if(empty($cart))
			redirect('transaksi/cart');

		$cart_items = array();
		$berat = 0;
		$quantity = 0;
		$total_harga = 0;
		// masukkan barang dalam cart ke dalam detail penjualan
		foreach ($cart as $item) {
			$this->db->set("id_penjualan", $id_penjualan);
			$this->db->set("id_produk", $item['id_produk']);
			$this->db->set("jumlah", $item['jumlah']);
			$this->db->set("harga_modal", $item['harga_modal']);
			$this->db->set("harga", $item['harga_jual']);
			$this->db->set("subtotal", $item['harga_jual'] * $item['jumlah']);
			$this->db->insert("tb_detail_penjualan");
			$berat += $item['berat'] * $item['jumlah'];
			$quantity += $item['jumlah'];//hitung jumlah barang
			$total_harga+=$item['harga_jual'] * $item['jumlah'];//dan harga barang

			$this->db->set("id_produk", $item['id_produk']);
			$this->db->set("jumlah", $item['jumlah']);
			$this->db->set("jatuh_tempo", 'NOW() + INTERVAL 1 DAY', FALSE);
			$this->db->insert("tb_dipesan");

			$new_item = array(
				'nama'=>$item['nama_produk'],
				'jumlah'=>$item['jumlah'],
				'harga_jual'=>$item['harga_jual'],
			);
			array_push($cart_items, $new_item);

			$this->db->set("stok", "stok - ".$item['jumlah'], FALSE);
			$this->db->where("id_produk",$item['id_produk']);
			$this->db->update("tb_produk");
		}

		//hapus barang di dalam shopping cart
		// $this->db->delete("tb_shopping_cart", array(
		// 	'id_user'=>$this->session->userdata('id_user')
		// )); 

		$kode_unik = rand(1,999);

		$this->db->select(array('id_kota', 'kurir'));
		$this->db->where('id_pengiriman', $id_pengiriman);
		$pengiriman = $this->db->get("tb_pengiriman")->row_array();

		//update jumlah dan total harga barang
		$this->db->set("total_quantity", $quantity);
		$this->db->set("total_harga", $total_harga);
		$ongkir = $this->get_ongkir($pengiriman['id_kota'], $berat, $pengiriman['kurir']);
		$this->db->set("ongkos_kirim", $ongkir->value);
		$this->db->set("total", $total_harga + $ongkir->value);
		$this->db->where("id_penjualan",$id_penjualan);
		$this->db->update("tb_penjualan");

		$this->db->set("total_harga", $total_harga + $ongkir->value + $kode_unik);
		$this->db->set("total_quantity", $quantity);
		$this->db->set("customer_id", $this->session->userdata('id_user'));
		$this->db->set("id_penjualan", $id_penjualan);
		$this->db->set("kode_unik", $kode_unik);
		$this->db->set("jumlah_dibayar", $total_harga + $ongkir->value + $kode_unik);
		$this->db->set("tanggal_berakhir", 'NOW() + INTERVAL 1 DAY', FALSE);
		$this->db->insert("tb_pembayaran");

		return array(
			"cart_items"=>$cart_items,
			"total"=>$total_harga,
			"ongkos_kirim"=>$ongkir->value, 
			"etd"=>$ongkir->etd,
			"quantity"=>$quantity, 
			"kode_unik"=>$kode_unik
		);
	}

	private function get_ongkir($id_kota_tujuan, $berat, $kurir){
		$this->load->model('user_model');
		$user = $this->user_model->get(
			array('id_kota'),
			$this->session->userdata('id_user')
		);
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=254&destination=".$id_kota_tujuan."&weight=".$berat."&courier=".$kurir,
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: be6d23888be8e0ee82cef8811ce3ce69"
			),
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			// echo "cURL Error #:" . $err;
			return null;
		} else {
			// echo $response;
			$ongkir = json_decode($response);
			return $ongkir->rajaongkir->results[0]->costs[0]->cost[0];
		}
	}

	function clear_unpaid(){
		$this->db->where("jatuh_tempo < CURDATE();");
		$order = $this->db->get("tb_dipesan")->result_array();

		foreach ($order as $item) {
			$this->db->set("stok", "stok + ".$item['jumlah'], FALSE);
			$this->db->where("id_produk",$item['id_produk']);
			$this->db->update("tb_produk");
		}

		$this->db->where("jatuh_tempo < CURDATE();");
		$this->db->delete("tb_dipesan");

		$this->db->where("tanggal_berakhir = CURDATE();");
		$this->db->join('tb_user', '`tb_pembayaran`.`customer_id` = `tb_user`.`id_user`');
		return $this->db->get("tb_pembayaran")->result_array();
	}

	function get_all($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get("tb_penjualan");
		return $query->result_array();
	}

	function get_all_detail($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('tb_detail_penjualan', '`tb_penjualan`.`id_penjualan` = `tb_detail_penjualan`.`id_penjualan`');
		$this->db->join('tb_produk', '`tb_detail_penjualan`.`id_produk` = `tb_produk`.`id_produk`');
		$query = $this->db->get("tb_penjualan");
		return $query->result_array();
	}

	function get_all_summary($select, $where, $group_by){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('tb_detail_penjualan', '`tb_penjualan`.`id_penjualan` = `tb_detail_penjualan`.`id_penjualan`');
		$this->db->join('tb_produk', '`tb_detail_penjualan`.`id_produk` = `tb_produk`.`id_produk`');
		$this->db->group_by($group_by);
		$query = $this->db->get("tb_penjualan");
		return $query->result_array();
	}

	function get_all_detail_customer($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('tb_detail_penjualan', '`tb_penjualan`.`id_penjualan` = `tb_detail_penjualan`.`id_penjualan`');
		$this->db->join('tb_produk', '`tb_detail_penjualan`.`id_produk` = `tb_produk`.`id_produk`');
		$this->db->join('tb_user', '`tb_penjualan`.`customer_id` = `tb_user`.`id_user`');
		$query = $this->db->get("tb_penjualan");
		return $query->result_array();
	}

	function get_all_active_payment($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->where('tanggal_berakhir > NOW()');
		$this->db->join('tb_penjualan', '`tb_penjualan`.`id_penjualan` = `tb_pembayaran`.`id_penjualan`');

		// $this->db->order_by('id_pembayaran', 'DESC');

		$query = $this->db->get("tb_pembayaran");
		return $query->result_array();
	}

	function get_all_payment($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('tb_penjualan', '`tb_penjualan`.`id_penjualan` = `tb_pembayaran`.`id_penjualan`');

		// $this->db->order_by('id_pembayaran', 'DESC');

		$query = $this->db->get("tb_pembayaran");
		return $query->result_array();
	}

	function get_all_customer_payment($select, $where=array()){
		$this->db->select($select);
		$this->db->where($where);
		// $this->db->order_by('id_pembayaran', 'DESC');
		$this->db->join('tb_user', '`tb_pembayaran`.`customer_id` = `tb_user`.`id_user`');
		$query = $this->db->get("tb_pembayaran");
		return $query->result_array();
	}

	function get_all_transaction($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('tb_user', '`tb_penjualan`.`customer_id` = `tb_user`.`id_user`');
		$this->db->join('tb_pembayaran', '`tb_penjualan`.`id_penjualan` = `tb_pembayaran`.`id_penjualan`');
		$query = $this->db->get("tb_penjualan");
		return $query->result_array();
	}

	function get_all_detail_transaction($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('tb_user', '`tb_penjualan`.`customer_id` = `tb_user`.`id_user`');
		$this->db->join('tb_detail_penjualan', '`tb_penjualan`.`id_penjualan` = `tb_detail_penjualan`.`id_penjualan`');
		$this->db->join('tb_produk', '`tb_detail_penjualan`.`id_produk` = `tb_produk`.`id_produk`');
		$query = $this->db->get("tb_penjualan");
		return $query->result_array();
	}

	function get_customer_payment($select, $id){
		$this->db->select($select);
		$this->db->where('id_pembayaran', $id);
		$this->db->join('tb_user', '`tb_pembayaran`.`customer_id` = `tb_user`.`id_user`');
		$query = $this->db->get("tb_pembayaran");
		return $query->row_array();
	}

	function confirm_payment($data){
		$this->db->set("bank", $data['bank']);
		$this->db->set("no_rekening", $data['no_rekening']);
		$this->db->set("nama_pengirim", $data['nama_pengirim']);
		$this->db->set("tanggal", date('Y-m-d', strtotime($data['tanggal'])));
		$this->db->set("jumlah", $data['jumlah']);
		$this->db->set("status_pembayaran", 1);
		$this->db->where(array('id_pembayaran'=>$data['id_pembayaran']));
		$this->db->update("tb_pembayaran");
	}

	function verify_payment($id_pembayaran){
		$this->db->set("status_pembayaran", 2);
		$this->db->where(array('id_pembayaran'=>$id_pembayaran));
		$this->db->update("tb_pembayaran");
	}

	function process_transaction($data){
		$this->db->set("status", 1);
		$this->db->set("resi", $data['resi']);
		$this->db->where(array('id_penjualan'=>$data['id_penjualan']));
		$this->db->update("tb_penjualan");

		$this->db->select("email");
		$this->db->join('tb_user', '`tb_penjualan`.`customer_id` = `tb_user`.`id_user`');
		$this->db->where(array('id_penjualan'=>$data['id_penjualan']));
		$query = $this->db->get("tb_penjualan");
		return array(
			'email'=>$query->row()->email,
			'resi'=>$data['resi']
		);
	}
}
