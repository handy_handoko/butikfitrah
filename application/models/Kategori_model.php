<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function set_data($data){
		if(isset($data['nama_kategori']))$this->db->set("nama_kategori", $data['nama_kategori']);
		if(isset($data['jenis_kategori']))$this->db->set("jenis_kategori", $data['jenis_kategori']);
	}

	function add($data = array()){
		$this->set_data($data);
		$this->db->insert("tb_kategori");
		$insert_id = $this->db->insert_id();
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return $insert_id;
		}
		return 0;
	}

	function update($data = array()){
		$this->set_data($data);
		$this->db->where("id_kategori",$data["id_kategori"]);
		$this->db->update("tb_kategori");

		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	function get_all($select=array(), $where=array()){
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get("tb_kategori");
		return $query->result_array();
	}

	function get($select, $id){
		$this->db->select($select);
		$this->db->where(array("tb_kategori.id_kategori"=>$id));
		$this->db->limit(1,0);
		
		$query = $this->db->get("tb_kategori");
		return $query->row_array();
	}

	function delete($id){
		$this->db->delete("tb_kategori", array("id_kategori" => $id)); 
	}

	function get_kategori(){
		$kategori = $this->get_all(array('id_kategori', 'nama_kategori', 'jenis_kategori'));
		$data=array(
			0=>array(),
			1=>array(),
		);
		foreach($kategori as $kat){
			array_push($data[$kat['jenis_kategori']], 
				anchor("transaksi/kategori/".$kat['id_kategori'], $kat['nama_kategori']));
		}
		return $data;
	}

	function get_data(){
		$kategori = $this->get_all(array('id_kategori', 'nama_kategori', 'jenis_kategori'));
		$data=array();
		foreach($kategori as $kat){
			$data[$kat['id_kategori']] = $kat['nama_kategori'];
		}
		return $data;
	}
}