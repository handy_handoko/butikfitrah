<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image_model extends CI_Model {

	public function insert($data = array()){
		$this->db->set("id_produk", $data['id_produk']);
		$this->db->set("nama_gambar", $data['nama_gambar']);
		$this->db->set("main_image", $data['main_image']);
		$this->db->insert("tb_gambar");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	public function delete($id_produk){
		$this->db->delete("tb_gambar", array("id_produk" => $id_produk)); 
	}
}