<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert($data = array()){
		$this->db->set("id_user", $this->session->userdata('id_user'));
		$this->db->set("id_produk", $data['id_produk']);
		$this->db->insert("tb_shopping_cart");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	public function update($data = array()){
		$this->db->set("jumlah", $data['jumlah']);
		$this->db->where("id_shopping_cart",$data["id_shopping_cart"]);
		$this->db->update("tb_shopping_cart");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	public function delete($id){
		$this->db->delete("tb_shopping_cart", array("id_shopping_cart" => $id)); 
	}

	public function get_all_cart($select = array()){
		$this->db->select($select);
		$this->db->join('tb_produk', 'tb_shopping_cart.id_produk = tb_produk.id_produk');
		// $this->db->join('tb_gambar', 'tb_produk.id_produk = tb_gambar.id_produk');
		$this->db->where(array('id_user'=>$this->session->userdata('id_user')));
		$query = $this->db->get("tb_shopping_cart");
		return $query->result_array();
	}
}