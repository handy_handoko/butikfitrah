<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengiriman_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert($data = array()){
		$this->db->set("nama_penerima", $data['nama_penerima']);
		$this->db->set("id_provinsi", $data['id_provinsi']);
		$this->db->set("id_kota", $data['id_kota']);
		$this->db->set("telepon", $data['telepon']);
		$this->db->set("kecamatan", $data['kecamatan']);
		$this->db->set("kelurahan", $data['kelurahan']);
		$this->db->set("alamat", $data['alamat']);
		$this->db->set("kurir", $data['kurir']);
		$this->db->insert("tb_pengiriman");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return $this->db->insert_id();
		}
		return null;
	}
}