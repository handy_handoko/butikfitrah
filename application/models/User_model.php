<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	function add($data = array()){
		$this->db->set("email", $data['email']);
		$this->db->set("password", password_hash($data['password'], PASSWORD_DEFAULT));
		$this->db->set("nama_lengkap", $data['nama_lengkap']);
		$this->db->set("role", 1);
		$insert_id = $this->db->insert("tb_user");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return $insert_id;
		}
		return 0;
	}

	public function register($data = array()){
		$this->db->set("email", $data['email']);
		$this->db->set("password", password_hash($data['password'], PASSWORD_DEFAULT));
		$this->db->set("nama_lengkap", $data['nama_lengkap']);
		$this->db->set("alamat", $data['alamat']);
		$this->db->set("telepon", $data['telepon']);
		$this->db->set("id_provinsi", $data['id_provinsi']);
		$this->db->set("id_kota", $data['id_kota']);
		$this->db->set("kecamatan", $data['kecamatan']);
		$this->db->set("kelurahan", $data['kelurahan']);
		$this->db->set("RW", $data['RW']);
		$this->db->set("RT", $data['RT']);
		$this->db->set("kodepos", $data['kodepos']);
		$this->db->insert("tb_user");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('success', 'register success');
			return true;
		}
		return false;
	}

	public function update($data = array()){
		$this->db->set("email", $data['email']);
		if($data['password'] != "")
			$this->db->set("password", password_hash($data['password'], PASSWORD_DEFAULT));
		$this->db->set("nama_lengkap", $data['nama_lengkap']);
		$this->db->set("alamat", $data['alamat']);
		$this->db->set("telepon", $data['telepon']);
		$this->db->set("id_provinsi", $data['id_provinsi']);
		$this->db->set("id_kota", $data['id_kota']);
		$this->db->set("kecamatan", $data['kecamatan']);
		$this->db->set("kelurahan", $data['kelurahan']);
		$this->db->set("RW", $data['RW']);
		$this->db->set("RT", $data['RT']);
		$this->db->set("kodepos", $data['kodepos']);
		$this->db->where("id_user",$data["id_user"]);
		$this->db->update("tb_user");
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	function get_all($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get("tb_user");
		return $query->result_array();
	}

	public function login($userdata, $select=array()){
		if( isset($select))
			$this->db->select($select);
		
		$this->db->where(array(
			"email"=>$userdata["email"]
		));
		$this->db->limit(1,0);
		$user = $this->db->get("tb_user")->row_array();

		if(password_verify($userdata["password"], $user["password"])){
			$user["password"]='';
			return $user;
		}
		return false;
	}

	function get($select, $id){
		$this->db->select($select);
		$this->db->where(array("tb_user.id_user"=>$id));
		$this->db->limit(1,0);
		
		$query = $this->db->get("tb_user");
		return $query->row_array();
	}
}