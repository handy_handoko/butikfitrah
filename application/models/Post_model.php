<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function set_data($data){
		if(isset($data['judul_post']))$this->db->set("judul_post", $data['judul_post']);
		if(isset($data['isi_artikel']))$this->db->set("isi_artikel", $data['isi_artikel']);
	}

	function add($data = array()){
		$this->set_data($data);
		$this->db->insert("tb_post");
		$insert_id = $this->db->insert_id();
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return $insert_id;
		}
		return 0;
	}

	function update($data = array()){
		$this->set_data($data);
		$this->db->where("id_post",$data["id_post"]);
		$this->db->update("tb_post");

		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	function get_all($select=array(), $where=array()){
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get("tb_post");
		return $query->result_array();
	}

	function get($select, $id){
		$this->db->select($select);
		$this->db->where(array("tb_post.id_post"=>$id));
		$this->db->limit(1,0);
		
		$query = $this->db->get("tb_post");
		return $query->row_array();
	}

	function delete($id){
		$this->db->delete("tb_post", array("id_post" => $id)); 
	}
}