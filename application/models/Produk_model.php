<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function set_data($data){
		if(isset($data['id_kategori']))$this->db->set("id_kategori", $data['id_kategori']);
		if(isset($data['nama_produk']))$this->db->set("nama_produk", $data['nama_produk']);
		if(isset($data['gambar']))$this->db->set("gambar", $data['gambar']);
		if(isset($data['harga_jual']))$this->db->set("harga_jual", $data['harga_jual']);
		if(isset($data['harga_modal']))$this->db->set("harga_modal", $data['harga_modal']);
		if(isset($data['stok']))$this->db->set("stok", $data['stok']);
		if(isset($data['berat']))$this->db->set("berat", $data['berat']);
		if(isset($data['deskripsi']))$this->db->set("deskripsi", $data['deskripsi']);
	}

	function add($data = array()){
		$this->set_data($data);
		$this->db->insert("tb_produk");
		$insert_id = $this->db->insert_id();
		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return $insert_id;
		}
		return 0;
	}

	function update($data = array()){
		$this->set_data($data);
		$this->db->where("id_produk",$data["id_produk"]);
		$this->db->update("tb_produk");

		if($this->db->affected_rows() > 0){
			$this->session->set_flashdata('notif', 'data saved');
			return true;
		}
		return false;
	}

	function get_all($select){
		$this->db->select($select);
		$query = $this->db->get("tb_produk");
		return $query->result_array();
	}

	function get_all_terbaru($select){
		$this->db->select($select);
		$this->db->limit(10, 0);
		$this->db->order_by('id_produk', 'DESC');
		$query = $this->db->get("tb_produk");
		return $query->result_array();
	}

	function get_all_produk($select, $where){
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get("tb_produk");
		return $query->result_array();
	}

	function get_array_disewakan(){
		$this->db->select("id_produk, nama_produk");
		$this->db->where(array('disewakan'=>1));
		$query = $this->db->get("tb_Produk");
		$data = array();
		foreach ($query->result_array() as $result) {
			$data[$result['id_Produk']] = $result['nama_Produk'];
		}
		return $data;
	}

	function search($select, $search_name){
		$this->db->select($select);
		$this->db->join('tb_gambar', 'tb_produk.id_produk = tb_gambar.id_produk');
		$this->db->where(array('main_image'=>1));
		$this->db->like('nama_produk', $search_name);
		$query = $this->db->get("tb_Produk");
		return $query->result_array();
	}

	function get_all_with_main_image($select){
		$this->db->select($select);
		$this->db->join('tb_gambar', 'tb_produk.id_produk = tb_gambar.id_produk');
		$this->db->where(array('main_image'=>1));
		$query = $this->db->get("tb_produk");
		return $query->result_array();
	}

	function get($select, $id){
		$this->db->select($select);
		$this->db->where(array("tb_produk.id_produk"=>$id));
		$this->db->limit(1,0);
		
		$query = $this->db->get("tb_produk");
		return $query->row_array();
	}

	function delete($id){
		$this->db->delete("tb_produk", array("id_produk" => $id)); 
	}
}