<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="robots" content="all,follow">
	<meta name="googlebot" content="index,follow,snippet,archive">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Obaju e-commerce template">
	<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
	<meta name="keywords" content="">

	<title>
		<?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?>
	</title>

	<meta name="keywords" content="">

	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

	<!-- styles -->
	<link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/owl.carousel.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/owl.theme.css'); ?>" rel="stylesheet">

	<!-- theme stylesheet -->
	<link href="<?php echo base_url('assets/css/style.default.css'); ?>" rel="stylesheet" id="theme-stylesheet">

	<!-- your stylesheet with modifications -->
	<link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">

	<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>

	<link rel="shortcut icon" href="favicon.png">



</head>

<body>
	<!-- *** TOPBAR ***
 _________________________________________________________ -->
	<div id="top">
		<div class="container">
			<div class="col-md-12" data-animate="fadeInDown">
				<ul class="menu">
					<?php if(!$this->session->userdata('id_user')){ ?>
					<li>
						<a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
					</li>
					<li>
						<?=anchor('user', 'Register')?>
					</li>
					<?php } else { ?>
					<li>
						<?=anchor("user/edit_profile", "Profile")?>
					</li>
					<li>
						<?=anchor('Transaksi/confirm', 'Konfirmasi pembayaran')?>
					</li>
					<li>
						<?=anchor('transaksi/process', 'Sedang diproses')?>
					</li>
					<li>
						<?=anchor('transaksi/sent', 'Barang dikirim')?>
					</li>
					<li>
						<?=anchor('user/logout', 'Logout')?>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
			<div class="modal-dialog modal-sm">

				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="Login">Login Pelanggan</h4>
					</div>
					<div class="modal-body">
						<?= form_open('user/login'); ?>
							<div class="form-group">
								<?=form_input('email', null, array("class"=>"form-control", "id"=>"email-modal", "placeholder"=>"email"));?>
							</div>
							<div class="form-group">
								<?=form_password('password', null, array("class"=>"form-control","id"=>"password-modal", "id"=>"password", "placeholder"=>"password"));?>
							</div>
							<p class="text-center">
								<button class="btn btn-primary">
									<i class="fa fa-sign-in"></i> Log in</button>
							</p>
						   <?=form_close();?>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- *** TOP BAR END *** -->

	<!-- *** NAVBAR ***
 _________________________________________________________ -->

	<div class="navbar navbar-default yamm" role="navigation" id="navbar">
		<div class="container">
			<div class="navbar-header">

				<a class="navbar-brand home" href="<?=base_url()?>" data-animate-hover="bounce">
					<img src="<?= base_url('assets/img/logo.png'); ?>" alt="Butik fitrah logo" class="hidden-xs">
					<img src="<?= base_url('assets/img/logo-small.png'); ?>" alt="Butik fitrah logo" class="visible-xs">
					<span class="sr-only">Butik fitrah - go to homepage</span>
				</a>
				<div class="navbar-buttons">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
						<span class="sr-only">Toggle navigation</span>
						<i class="fa fa-align-justify"></i>
					</button>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
						<span class="sr-only">Toggle search</span>
						<i class="fa fa-search"></i>
					</button>
					<a class="btn btn-default navbar-toggle" href="basket.html">
						<i class="fa fa-shopping-cart"></i>
						<span class="hidden-xs">3 items in cart</span>
					</a>
				</div>
			</div>
			<!--/.navbar-header -->

			<div class="navbar-collapse collapse" id="navigation">

				<ul class="nav navbar-nav navbar-left">
					<li class="active">
						<?=anchor('', 'Home')?>
					</li>
					<li class="dropdown yamm-fw">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Pria
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="yamm-content">
									<div class="row">
										<div class="col-sm-3">
											<h5>Pakaian pria</h5>
											<ul>
												<?php foreach ($kategori[1] as $kat){ ?>
													<li>
														<?=$kat?>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
								<!-- /.yamm-content -->
							</li>
						</ul>
					</li>

					<li class="dropdown yamm-fw">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Wanita
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="yamm-content">
									<div class="row">
										<div class="col-sm-3">
											<h5>Pakaian Wanita</h5>
											<ul>
												<?php foreach ($kategori[0] as $kat){ ?>
													<li>
														<?=$kat?>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
								<!-- /.yamm-content -->
							</li>
						</ul>
					</li>

					
			</div>
			<!--/.nav-collapse -->

			<div class="navbar-buttons">

				<div class="navbar-collapse collapse right" id="basket-overview">
					<a href="<?=site_url('transaksi/cart')?>" class="btn btn-primary navbar-btn">
						<i class="fa fa-shopping-cart"></i>
						<span class="hidden-sm">Keranjang belanja</span>
					</a>
				</div>
				<!--/.nav-collapse -->
			</div

		</div>
		<!-- /.container -->
	</div>
	<!-- /#navbar -->

	<!-- *** NAVBAR END *** -->
	<div id="all">
