<!DOCTYPE html>
<html>
	<body>
		Laporan Penjualan Butik Fitrah<br/>
		<br/>
		<br/>
		<table border="1" width="100%">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Nama Produk</th>
					<th>Jumlah</th>
					<th>Harga</th>
					<th>Subtotal</th>
					<th>Harga Modal</th>
					<th>Keuntungan</th>
					<th>Alamat</th>
					<th>Telepon</th>
				</tr>
			</thead>
			<tbody>
			<?php $total_keuntungan = 0;
			foreach ($data as $no=>$d) {
			$total_keuntungan += ($d['harga'] - $d['harga_modal']) * $d['jumlah']; ?>
				<tr>
					<td><?=$no+1?></td>
					<td><?=$d['tanggal_penjualan']?></td>
					<td><?=$d['nama_produk']?></td>
					<td><?=$d['jumlah']?></td>
					<td><?=$d['harga']?></td>
					<td><?=$d['subtotal']?></td>
					<td><?=$d['harga_modal'] * $d['jumlah']?></td>
					<td><?=($d['harga'] - $d['harga_modal']) * $d['jumlah']?></td>
					<th><?=$d['alamat']?></th>
					<th><?=$d['telepon']?></th>
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<th>Tanggal</th>
					<th>Nama Produk</th>
					<th>Jumlah</th>
					<th>Harga</th>
					<th>Subtotal</th>
					<th>Keuntungan</th>
				</tr>
			</tfoot>
		</table>
		<div>
			<table style="float: right;">
				<tr>
					<td align="center"><strong> Total Keuntungan &nbsp;</strong></td>
					<td align="center"><strong><?=$total_keuntungan;?></strong></td>
				</tr>
				<tr>
					<td align="center">Tertanda</td>
				</tr>
				<tr height="150">
					<td align="center">_____________</td>
				</tr>
			</table>
		</div>
	</body>
</html>