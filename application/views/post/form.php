<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?></title>

	<!-- Bootstrap Core CSS -->
	<link href=<?=base_url("/assets/vendor/bootstrap/css/bootstrap.min.css")?> rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.css")?> rel="stylesheet">

	<!-- Custom CSS -->
	<link href=<?=base_url("/assets/dist/css/sb-admin-2.css")?> rel="stylesheet">

	<!-- Custom Fonts -->
	<link href=<?=base_url("/assets/vendor/font-awesome/css/font-awesome.min.css")?> rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<div id="wrapper">
		<?php $this->load->view("admin_menu");?>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Data Artikel</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Form Artikel
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<?=form_open_multipart('post/save'); ?>
									<?php if(isset($data['id_kategori']))
										echo form_hidden('id_kategori', $data['id_kategori']);?>
										<div class="form-group">
											<label>Judul Artikel</label>
											<?=form_input('judul_post', isset($data['judul_post'])?$data['judul_post']:'', array('class'=>'form-control', 'id'=>'judul_post','placeholder'=>'Masukkan judul post'));?>
										</div>
										<div class="form-group">
											<label>Isi Artikel</label>
											<?=form_textarea('isi_artikel', isset($data['isi_artikel'])?$data['isi_artikel']:'', array('class'=>'form-control', 'id'=>'isi_artikel'));?>
										</div>
										<button type="submit" class="btn btn-success">Submit</button>
										<button type="reset" class="btn btn-default">Reset</button>
									<?=form_close()?>
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src=<?=base_url("/assets/vendor/jquery/jquery.min.js")?>></script>

	<!-- Bootstrap Core JavaScript -->
	<script src=<?=base_url("/assets/vendor/bootstrap/js/bootstrap.min.js")?>></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.js")?>></script>

	<!-- Custom Theme JavaScript -->
	<script src=<?=base_url("/assets/dist/js/sb-admin-2.js")?>></script>
	
	<script>
	$(document).ready(function() {
		$('#disewakan').click(function(){
			$('.sewa').removeAttr('disabled');
		});
	});
	</script>
</body>

</html>

