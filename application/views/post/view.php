<div id="content">
	<div class="container">

		<div class="col-sm-12">

			<ul class="breadcrumb">

				<li><a href="<?=base_url()?>">Home</a>
				</li>
				</li>
				<li>Blog post</li>
			</ul>
		</div>

		<div class="col-sm-9" id="blog-post">


			<div class="box">

				<h1><?=$post['judul_post']?></h1>

				<div id="post-content">

					<?=$post['isi_artikel']?>

				</div>
				<!-- /#post-content -->

			</div>
			<!-- /.box -->
		</div>
		<!-- /#blog-post -->

	</div>
	<!-- /.container -->
</div>
<!-- /#content -->