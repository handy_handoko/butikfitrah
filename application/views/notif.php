<?php if($this->session->flashdata('success') != null) { ?>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script type="text/javascript">
		$(function() {
			swal({
				icon: "success",
				title: "<?=$this->session->flashdata('success')?>",
			});
		});
	</script>
<?php } ?>
<?php if($this->session->flashdata('error') != null) { ?>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script type="text/javascript">
		$(function() {
			swal({
				icon: "error",
				title: "<?=$this->session->flashdata('error')?>",
			});
		});
	</script>
<?php } ?>