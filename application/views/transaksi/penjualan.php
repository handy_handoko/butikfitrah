<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?></title>

	<!-- Bootstrap Core CSS -->
	<link href=<?=base_url("/assets/vendor/bootstrap/css/bootstrap.min.css")?> rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.css")?> rel="stylesheet">

	<!-- DataTables CSS -->
	<link href=<?=base_url("/assets/vendor/datatables-plugins/dataTables.bootstrap.css")?> rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href=<?=base_url("/assets/vendor/datatables-responsive/dataTables.responsive.css")?> rel="stylesheet">

	<link href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href=<?=base_url("/assets/dist/css/sb-admin-2.css")?> rel="stylesheet">

	<!-- Custom Fonts -->
	<link href=<?=base_url("/assets/vendor/font-awesome/css/font-awesome.min.css")?> rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<div id="wrapper">
		<?php $this->load->view("admin_menu");?>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Daftar transaksi penjualan</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Daftar transaksi penjualan
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Nama Pembeli</th>
										<th>Total</th>
										<th>Alamat</th>
										<th>Telepon</th>
										<th>Detail</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($transaksi as $d) { ?>
									<tr>
										<td><?=$d['tanggal_penjualan']?></td>
										<td><?=$d['nama_lengkap']?></td>
										<td><?=$d['total']?></td>
										<td><?=$d['alamat']?></td>
										<td><?=$d['telepon']?></td>
										<td><?=anchor("transaksi/detail_transaction/".$d['id_penjualan'], "Detail transaksi")?></td>
										<td><?php
											if($d['status'] == 0){
												echo "Belum diproses";
											} else {
												echo "Sudah diproses";
											}
										?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
							<!-- /.table-responsive -->
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
					<div class="panel panel-default">
						<div class="panel-heading">
							Daftar transaksi selesai
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Nama Pembeli</th>
										<th>Total</th>
										<th>Alamat</th>
										<th>Telepon</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($sent as $s) { ?>
									<tr>
										<td><?=$s['tanggal_penjualan']?></td>
										<td><?=$s['nama_lengkap']?></td>
										<td><?=$s['total']?></td>
										<td><?=$s['alamat']?></td>
										<td><?=$s['telepon']?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
							<!-- /.table-responsive -->
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src=<?=base_url("/assets/vendor/jquery/jquery.min.js")?>></script>

	<!-- Bootstrap Core JavaScript -->
	<script src=<?=base_url("/assets/vendor/bootstrap/js/bootstrap.min.js")?>></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.js")?>></script>

	<!-- DataTables JavaScript -->
	<script src=<?=base_url("/assets/vendor/datatables/js/jquery.dataTables.min.js")?>></script>
	<script src=<?=base_url("/assets/vendor/datatables-plugins/dataTables.bootstrap.min.js")?>></script>
	<script src=<?=base_url("/assets/vendor/datatables-responsive/dataTables.responsive.js")?>></script>
	<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
	

	<!-- Custom Theme JavaScript -->
	<script src=<?=base_url("/assets/dist/js/sb-admin-2.js")?>></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			dom: 'Bfrtip',
			buttons: [
				'pdf',
				'excel',
				'print'
			]
		});
	});
	</script>

</body>

</html>
