<div id="content">
	<div class="container">

		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Shopping cart</li>
			</ul>
		</div>

		<div class="col-md-9" id="basket">

			<div class="box">
				<?= form_open('transaksi/updateCart')?>
					<h1>Keranjang belanja</h1>
					<p class="text-muted"><?=count($data)?> barang ada dalam keranjang.</p>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th colspan="2">Nama barang</th>
									<th>Stok</th>
									<th>Jumlah</th>
									<th>Harga Satuan</th>
									<th colspan="2">Total</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$total = 0;
								foreach($data as $produk){?>
									<tr>
										<td>
											<a href="#">
												<img src="<?=base_url('upload/product/'.$produk['gambar'])?>" alt="<?=$produk['nama_produk']?>">
											</a>
										</td>
										<td>
											<a href="#"><?=$produk['nama_produk']?></a>
											<?=form_hidden('id_shopping_cart[]', $produk['id_shopping_cart']);?>
										</td>
										<td>
											<?=$produk['stok']?>
										</td>
										<td>
											<?=form_input('jumlah[]', $produk['jumlah'], array("class"=>"form-control"));?>
										</td>
										<td>Rp. <?=number_format($produk['harga_jual'],2,",",".");?></td>
										<td>Rp. <?=number_format($produk['harga_jual']*$produk['jumlah'],2,",",".");?></td>
										<td>
											<a href="<?=site_url('transaksi/deleteFromCart/'.$produk['id_shopping_cart'])?>">
												<i class="fa fa-trash-o"></i>
											</a>
										</td>
									</tr>
								<?php 
								$total += $produk['harga_jual']*$produk['jumlah'];
								} 
								?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4">Total</th>
									<th colspan="2">Rp. <?=number_format($total,2,",",".");?></th>
								</tr>
							</tfoot>
						</table>

					</div>
					<!-- /.table-responsive -->

					<div class="box-footer">
						<div class="pull-left">
							<?=anchor(null, '<i class="fa fa-chevron-left"></i> Lanjut belanja', array("class"=>"btn btn-default"));?>
						</div>
						<div class="pull-right">
							<button class="btn btn-default" type="submit">
								<i class="fa fa-refresh"></i> Update
							</button>
							<?=anchor('transaksi/shipping', "Proses pesanan <i class='fa fa-chevron-right'></i>", array("class"=>"btn btn-primary"));?>
						</div>
					</div>

				<?= form_close(); ?>

			</div>
			<!-- /.box -->

		</div>
		<!-- /.col-md-9 -->

		<div class="col-md-3">
			<div class="box" id="order-summary">
				<div class="box-header">
					<h3>Ringkasan pesanan</h3>
				</div>
				<p class="text-muted">Berikut total belanjaan anda.</p>

				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td>Order subtotal</td>
								<th>Rp. <?=number_format($total,2,",",".");?></th>
							</tr>
							<tr class="total">
								<td>Total</td>
								<th>Rp. <?=number_format($total,2,",",".");?></th>
							</tr>
						</tbody>
					</table>
				</div>

			</div>
		</div>
		<!-- /.col-md-3 -->

	</div>
	<!-- /.container -->
</div>
<!-- /#content -->