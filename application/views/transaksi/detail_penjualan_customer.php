<div id="content">
	<div class="container">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Daftar Pesanan</li>
			</ul>
		</div>

		<div class="col-md-12">
		Tanggal Pesanan: <?=$tanggal_penjualan?> <br/>
		Ongkos Kirim: <?=$ongkos_kirim?> <br/>

			<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<tr>
						<td>Nama Produk</td>
						<td>Jumlah</td>
						<td>Harga</td>
						<td>Subtotal</td>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($order_detail as $d) { ?>
					<tr>
						<td><?=$d['nama_produk']?></td>
						<td><?=$d['jumlah']?></td>
						<td><?=number_format($d['harga'],0,",",".")?></td>
						<td><?=number_format($d['subtotal'],0,",",".")?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->
