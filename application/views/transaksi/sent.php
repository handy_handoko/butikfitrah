<div id="content">
	<div class="container">

		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Daftar Pesanan Terkirim</li>
			</ul>
		</div>

		<div class="col-md-9">
			<div class="box">
				<form method="post" action="checkout4.html">
					<h1>Daftar Pesanan Terkirim</h1>
					<div class="content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Tanggal transaksi</th>
										<th>Jumlah dibayar</th>
										<th>No. Resi</th>
										<th>Konfirmasi</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($orders as $order) {?>
									<tr>
										<td>
											<?=$order['tanggal_penjualan']; ?>
										</td>
										<td>
											<?=$order['total']; ?>
										</td>
										<td>
											<?=$order['resi']; ?>
										</td>
										<td>
											<?=anchor('transaksi/confirmsent/'.$order['id_penjualan'], 'Barang diterima'); ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.content -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->