<div id="content">
	<div class="container">

		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Daftar Pesanan Sedang Diproses</li>
			</ul>
		</div>

		<div class="col-md-9">
			<div class="box">
				<form method="post" action="checkout4.html">
					<h1>Daftar Pesanan Diproses</h1>
					<div class="content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Tanggal transaksi</th>
										<th>Jumlah dibayar</th>
										<th>Detail Barang</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($orders as $order) {?>
									<tr>
										<td>
											<?=$order['tanggal_penjualan']; ?>
										</td>
										<td>
											<?=$order['total']; ?>
										</td>
										<td>
											<?=anchor('transaksi/invoicedetail/'.$order['id_penjualan'], 'Detail'); ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.content -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->