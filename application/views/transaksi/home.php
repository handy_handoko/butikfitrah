<div id="content">
	<div class="container">
		<div class="col-md-12">
			<div id="main-slider">
				<div class="item">
					<img src="<?=base_url('assets/img/main-slider1.jpg')?>" alt="" class="img-responsive">
				</div>
				<div class="item">
					<img class="img-responsive" src="<?=base_url('assets/img/main-slider2.jpg')?>" alt="">
				</div>
				<div class="item">
					<img class="img-responsive" src="<?=base_url('assets/img/main-slider3.jpg')?>" alt="">
				</div>
				<div class="item">
					<img class="img-responsive" src="<?=base_url('assets/img/main-slider4.jpg')?>" alt="">
				</div>
			</div>
			<!-- /#main-slider -->
		</div>
	</div>

	<!-- *** HOT PRODUCT SLIDESHOW ***
 _________________________________________________________ -->
	<div id="hot">

		<div class="box">
			<div class="container">
				<div class="col-md-12">
					<h2>Produk terbaru</h2>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="product-slider">
				<?php foreach ($produk as $p) {?>
					<div class="item">
						<div class="product">
							<div class="flip-container">
								<div class="flipper">
									<div class="front">
										<a href="<?=site_url('transaksi/detail/'.$p['id_produk'])?>">
											<img src="<?=base_url('upload/product/'.$p['gambar'])?>" alt="" class="img-responsive">
										</a>
									</div>
									<div class="back">
										<a href="<?=site_url('transaksi/detail/'.$p['id_produk'])?>">
											<img src="<?=base_url('upload/product/'.$p['gambar'])?>" alt="" class="img-responsive">
										</a>
									</div>
								</div>
							</div>
							<a href="<?=site_url('transaksi/detail/'.$p['id_produk'])?>" class="invisible">
								<img src="<?=base_url('upload/product/'.$p['gambar'])?>" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3>
									<?=anchor('transaksi/detail/'.$p['id_produk'], $p['nama_produk'])?>
								</h3>
								<p class="price">Rp. <?=number_format($p['harga_jual'],2,",",".");?></p>
							</div>
							<!-- /.text -->
						</div>
						<!-- /.product -->
					</div>
				<?php } ?>
			</div>
			<!-- /.product-slider -->
		</div>
		<!-- /.container -->

	</div>
	<!-- /#hot -->

	<!-- *** HOT END *** -->

	<!-- *** BLOG HOMEPAGE ***
 _________________________________________________________ -->

	<div class="box text-center" data-animate="fadeInUp">
		<div class="container">
			<div class="col-md-12">
				<h3 class="text-uppercase">Artikel kami</h3>

			</div>
		</div>
	</div>

	<div class="container">

		<div class="col-md-12" data-animate="fadeInUp">

			<div id="blog-homepage" class="row">
				<?php foreach ($post as $key => $value) { ?>
					<div class="col-sm-6">
						<div class="post">
							<h4>
								<a href="post.html"><?=$value['judul_post']?></a>
							</h4>
							<hr>
							<?php $string = strip_tags($value['isi_artikel']);
								if (strlen($string) > 500) {

								    // truncate string
								    $stringCut = substr($string, 0, 500);
								    $endPoint = strrpos($stringCut, ' ');

								    //if the string doesn't contain any space then it will cut without word basis.
								    $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
								    $string .= anchor('post/view/'.$value['id_post'], '... Read More');
								}
								echo $string; ?>
						</div>
					</div>
				<?php } ?>
			</div>
			<!-- /#blog-homepage -->
		</div>
	</div>
	<!-- /.container -->

	<!-- *** BLOG HOMEPAGE END *** -->

</div>
<!-- /#content -->