<?php $template_path = "/assets/template/Company/"; ?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?></title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href=<?=base_url($template_path."css/bootstrap.min.css")?>>
	<link rel="stylesheet" href=<?=base_url($template_path."css/font-awesome.min.css")?>>
	<link rel="stylesheet" href=<?=base_url($template_path."css/animate.css")?>>
	<link rel="stylesheet" href=<?=base_url($template_path."css/prettyPhoto.css")?>>
	<link rel="stylesheet" href=<?=base_url($template_path."css/style.css")?> />

	<link rel="stylesheet" type="text/css" href=<?=base_url($template_path."slick/slick.css")?>/>
	<link rel="stylesheet" type="text/css" href=<?=base_url($template_path."slick/slick-theme.css")?>/>
</head>

<body>
		
	<?php $this->load->view("user_menu");?>

	<section id="main-slider" class="no-margin">
		<div class="carousel slide">
			<div class="carousel-inner">
				<div class="item active" style="background-image: url(<?=base_url($template_path."images/slider/bg1.jpg")?>)">
					<div class="container">
						<div class="row slide-margin">
							<div class="col-sm-6">
								<div class="carousel-content">
									<h2 class="animation animated-item-1">Isam<span>Commerce</span></h2>
									<p class="animation animated-item-2">IsamCommerce adalah toko online yang bergerak pada bidang penjualan alat-alat kesehatan. Untuk beberapa produk, kami juga menyediakan produk kesehatan yang dapat disewakan.</p>
									<a class="btn-slide animation animated-item-3" href="#forsale">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 hidden-xs animation animated-item-4">
								<div class="slider-img">
									<img src="<?=base_url($template_path."images/slider/img3.png")?>" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/.item-->
			</div>
			<!--/.carousel-inner-->
		</div>
		<!--/.carousel-->
	</section>
	<!--/#main-slider-->
	
	<div class="container" style="width: 100%;">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<img src="<?=base_url('assets/images/alatkesehatan.png')?>" alt="Los Angeles" style="width:100%;">
			</div>

			<div class="item">
				<img src="<?=base_url('assets/images/kirim.png')?>" alt="Chicago" style="width:100%;">
			</div>

			<div class="item">
				<img src="<?=base_url('assets/images/harga.png')?>" alt="New york" style="width:100%;">
			</div>
		</div>

		<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>

	<div class="forsale" id="forsale">
		<div class="container">
			<div class="text-center">
				<h2>Barang dijual</h2>
			</div>
			<?php foreach ($data as $d) { ?>
				<div class="col-md-3 wow fadeInDown item" data-wow-duration="1000ms" data-wow-delay="300ms">
					<img src="<?=base_url('upload/product/'.$d['nama_gambar']);?>" class="img-responsive" style="min-height: 300px;"/>
					<h3><?=$d['nama_alkes']?></h3>
					<?=anchor("transaksi/addToCart/".$d['id_alkes'], "Beli", array("class"=>"btn btn-success"))?>
				</div>
			<?php } ?>
		</div>
	</div>

	<div class="forrent" id="forrent">
		<div class="container">
			<div class="text-center">
				<h2>Barang disewakan</h2>
			</div>
			<?php foreach ($data as $d) { 
				if ($d['disewakan']==1) {?>
				<div class="col-md-3 wow fadeInDown product" data-wow-duration="1000ms" data-wow-delay="300ms">
					<img src="<?=base_url('upload/product/'.$d['nama_gambar']);?>" class="img-responsive" style="min-height: 300px;"/>
					<h3><?=$d['nama_alkes']?></h3>
					<p>
						Sewa Harian : <?=$d['harga_sewa_harian']?> <br/>
						Sewa Mingguan : <?=$d['harga_sewa_mingguan']?> <br/>
						Sewa Bulanan : <?=$d['harga_sewa_bulanan']?>
					</p>
				</div>
			<?php } }?>
		</div>
	</div>

	<footer>
		<div class="footer">
			<div class="container">
				<div class="social-icon">
					<div class="col-md-4">
						<ul class="social-network">
							<li><a href="#" class="fb tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#" class="twitter tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" class="gplus tool-tip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#" class="linkedin tool-tip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#" class="ytube tool-tip" title="You Tube"><i class="fa fa-youtube-play"></i></a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-4 col-md-offset-4">
					<div class="copyright">
						&copy; Company Theme. All Rights Reserved.
						<div class="credits">
							<!--
								All the links in the footer should remain intact.
								You can delete the links only if you purchased the pro version.
								Licensing information: https://bootstrapmade.com/license/
								Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Company
							-->
							<a href="https://bootstrapmade.com/bootstrap-business-templates/">Bootstrap Business Templates</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
							<br/>Created by Sheli.
						</div>
					</div>
				</div>
			</div>

			<div class="pull-right">
				<a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
			</div>
		</div>
	</footer>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?=base_url($template_path.'js/jquery-2.1.1.min.js')?>"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?=base_url($template_path.'js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url($template_path.'js/jquery.prettyPhoto.js')?>"></script>
	<script src="<?=base_url($template_path.'js/jquery.isotope.min.js')?>"></script>
	<script src="<?=base_url($template_path.'js/wow.min.js')?>"></script>
	<script src="<?=base_url($template_path.'js/functions.js')?>"></script>

	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?=base_url($template_path.'slick/slick.min.js')?>"></script>
	<script type="text/javascript">
		$(function() {
			$('.slide').slick();
		});
	</script>
</body>
</html>