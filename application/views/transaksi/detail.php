<div id="content">
	<div class="container">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<?=anchor(null, 'Home');?>
				</li>
				<li>
					<a href="#">Ladies</a>
				</li>
				<li>
					<a href="#">Tops</a>
				</li>
				<li>White Blouse Armani</li>
			</ul>
		</div>

		<div class="col-md-12">

			<div class="row" id="productMain">
				<div class="col-sm-6">
					<div id="mainImage">
						<img src="<?=base_url('upload/product/'.$data['gambar'])?>" alt="" class="img-responsive">
					</div>

				</div>
				<div class="col-sm-6">
					<div class="box">
						<h1 class="text-center"><?=$data['nama_produk']?></h1>
						<p class="goToDescription">
							<a href="#details" class="scroll-to">Lihat detail</a>
						</p>
						<p class="price">Rp. <?=number_format($data['harga_jual'],2,",",".");?></p>

						<p class="text-center buttons">
							<?=anchor('transaksi/addToCart/'. $data['id_produk'], '<i class="fa fa-shopping-cart"></i> Tambah ke keranjang belanja', array("class"=>"btn btn-primary"));?>
						</p>
					</div>

					<div class="row" id="thumbs">
						<div class="col-xs-4">
							<a href="<?=base_url('upload/product/'.$data['gambar'])?>" class="thumb">
								<img src="<?=base_url('upload/product/'.$data['gambar'])?>" alt="" class="img-responsive">
							</a>
						</div>
						<!-- <div class="col-xs-4">
							<a href="<?=base_url('upload/product/'.$data['gambar'])?>" class="thumb">
								<img src="<?=base_url('upload/product/'.$data['gambar'])?>" alt="" class="img-responsive">
							</a>
						</div>
						<div class="col-xs-4">
							<a href="<?=base_url('upload/product/'.$data['gambar'])?>" class="thumb">
								<img src="<?=base_url('upload/product/'.$data['gambar'])?>" alt="" class="img-responsive">
							</a>
						</div> -->
					</div>
				</div>

			</div>


			<div class="box" id="details">
				<p>
					<h4>Detail produk</h4>
					<?=$data['deskripsi']?>
			</div>

		</div>
		<!-- /.col-md-9 -->
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->