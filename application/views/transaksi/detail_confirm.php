<div id="content">
	<div class="container">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Daftar Pesanan</li>
			</ul>
		</div>

		<div class="col-md-12">
		Tanggal Pesanan: <?=$tanggal_penjualan?> <br/>

			<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<tr>
						<td>Nama Produk</td>
						<td>Jumlah</td>
						<td>Harga</td>
						<td>Subtotal</td>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($order_detail as $d) { ?>
					<tr>
						<td><?=$d['nama_produk']?></td>
						<td><?=$d['jumlah']?></td>
						<td><?=number_format($d['harga'],0,",",".")?></td>
						<td><?=number_format($d['subtotal'],0,",",".")?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>


			<div class="box">
				<?=form_open('transaksi/process_confirm'); ?>
					<h1>Daftar Pesanan</h1>
					<div class="content">
							<?=form_hidden('id_pembayaran', $id_pembayaran);?>
							<div class="form-group">
								<label>Bank</label>
								<?=form_input('bank', '', array('class'=>'form-control', 'id'=>'bank','placeholder'=>'Masukkan nama bank'));?>
							</div>
							<div class="form-group">
								<label>No rekening pengirim</label>
								<?=form_input('no_rekening', '', array('class'=>'form-control', 'id'=>'no_rekening','placeholder'=>'Masukkan no rekening'));?>
							</div>
							<div class="form-group">
								<label>Nama pengirim</label>
								<?=form_input('nama_pengirim', '', array('class'=>'form-control', 'id'=>'nama_pengirim'));?>
							</div>
							<div class="form-group">
								<label>Tanggal transfer</label>
								<?=form_input('tanggal', '', array('class'=>'form-control datepicker', 'id'=>'tanggal', 'data-provide'=>'datepicker', 'data-date-format'=>'dd/mm/yyyy', 'required'=>''));?>
							</div>
							<div class="form-group">
								<label>Jumlah ongkir</label>
								<?=form_input('ongkir', $ongkos_kirim, array('class'=>'form-control', 'id'=>'ongkir', 'readonly'=>''));?>
							</div>
							<div class="form-group">
								<label>Jumlah total yang harus dibayar</label>
								<?=form_input('jumlah', $jumlah_dibayar, array('class'=>'form-control', 'id'=>'jumlah', 'readonly'=>''));?>
							</div>
							<button type="submit" class="btn btn-success">Konfirmasi</button>
							<button type="reset" class="btn btn-default">Reset</button>
					</div>
					<!-- /.content -->
				<?=form_close()?>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->
