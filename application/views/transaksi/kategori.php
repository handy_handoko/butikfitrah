<div id="content">
	<div class="container">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li><a href="#">Home</a>
				</li>
				<li>Ladies</li>
			</ul>
		</div>

		<div class="col-md-12">
			<div class="row products">
				<?php foreach($produk as $p){ ?>
					<div class="col-md-3 col-sm-4">
						<div class="product">
							<div class="flip-container">
								<div class="flipper">
									<div class="front">
										<a href="<?=site_url('transaksi/detail/'.$p['id_produk'])?>">
											<img src="<?=base_url('upload/product/'.$p['gambar'])?>" alt="" class="img-responsive">
										</a>
									</div>
									<div class="back">
										<a href="<?=site_url('transaksi/detail/'.$p['id_produk'])?>">
											<img src="<?=base_url('upload/product/'.$p['gambar'])?>" alt="" class="img-responsive">
										</a>
									</div>
								</div>
							</div>
							<a href="<?=site_url('transaksi/detail/'.$p['id_produk'])?>" class="invisible">
								<img src="<?=base_url('upload/product/'.$p['gambar'])?>" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3>
									<?=anchor('transaksi/detail/'.$p['id_produk'], $p['nama_produk'])?>
								</h3>
								<p class="price">Rp. <?=number_format($p['harga_jual'],2,",",".");?></p>
							</div>
							<!-- /.text -->
						</div>
						<!-- /.product -->
					</div>
				<?php } ?>
			</div>
			<!-- /.products -->

		</div>
		<!-- /.col-md-9 -->
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->