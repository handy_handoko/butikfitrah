<div id="content">
	<div class="container">

		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Pengiriman</li>
			</ul>
		</div>

		<div class="col-md-8 col-md-offset-2" id="shipping">

			<div class="box">
				<?= form_open('transaksi/process_shipping')?>
					<div class="form-group">
						<label for="nama_penerima">Nama</label>
						<?=form_input('nama_penerima', $recipient['nama_penerima'], array('class'=>'form-control', 'id'=>'nama_penerima', 'placeholder'=>'Nama Penerima'));?>
					</div>
					<div class="form-group">
						<label for="id_provinsi">Provinsi</label>
						<?=form_dropdown('id_provinsi', $provinsi, $recipient['id_provinsi'], array('class'=>'form-control', 'id'=>'id_provinsi'));?>
					</div>
					<div class="form-group">
						<label for="id_kota">Kota</label>
						<?=form_dropdown('id_kota', $kota, $recipient['id_kota'], array('class'=>'form-control', 'id'=>'id_kota', 'placeholder'=>'Kota'));?>
					</div>
					<div class="form-group">
						<label for="telepon">Telepon</label>
						<?=form_input('telepon', $recipient['telepon'], array('class'=>'form-control', 'id'=>'telepon', 'placeholder'=>'Telepon'));?>
					</div>
					<div class="form-group">
						<label for="kecamatan">Kecamatan</label>
						<?=form_input('kecamatan', $recipient['kecamatan'], array('class'=>'form-control', 'id'=>'kecamatan', 'placeholder'=>'Kecamatan'));?>
					</div>
					<div class="form-group">
						<label for="kelurahan">Kelurahan</label>
						<?=form_input('kelurahan', $recipient['kelurahan'], array('class'=>'form-control', 'id'=>'kelurahan', 'placeholder'=>'Telepon'));?>
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<?=form_input('alamat', $recipient['alamat'], array('class'=>'form-control', 'id'=>'alamat', 'placeholder'=>'Alamat'));?>
					</div>
					<div class="form-group">
						<?=form_radio('kurir', 'jne', true, array('id'=>'jne', 'placeholder'=>'Kurir'));?>
						<label for="jne">JNE</label>
						<?=form_radio('kurir', 'pos', false, array('id'=>'pos', 'placeholder'=>'Kurir'));?>
						<label for="pos">POS Indonesia</label>
						<?=form_radio('kurir', 'tiki', false, array('id'=>'tiki', 'placeholder'=>'Kurir'));?>
						<label for="tiki">TIKI</label>
					</div>


					<div class="box-footer">
						<div class="pull-right">
							<button class="btn btn-primary" type="submit">
								Submit
							</button>
						</div>
					</div>

				<?= form_close(); ?>

			</div>
			<!-- /.box -->

		</div>
		<!-- /.col-md-9 -->

	</div>
	<!-- /.container -->
</div>
<!-- /#content -->