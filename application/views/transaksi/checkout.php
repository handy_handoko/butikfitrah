<div id="content">
	<div class="container">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Review pesanan</li>
			</ul>
		</div>

		<div class="col-md-12" id="transfer-info">
			<div class="box">
				<div class="content">
					<div class="table-responsive">
							Silahkan transfer sejumlah <?=$total + $kode_unik + $ongkos_kirim ?> ke:
							<p>
								 bank BRI<br/>
								 Cabang perintis kemerdekaan<br/>
								 no rekening 341401031880536<br/>
								 atas nama Butik Fitrah
							</p> 
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.content -->
			</div>
			<!-- /.box -->
		</div>

		<div class="col-md-9" id="checkout">
			<div class="box">
				<form method="post" action="checkout4.html">
					<h1>Pesanan anda</h1>
					<div class="content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Nama barang</th>
										<th>Jumlah</th>
										<th>Harga satuan</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<?php $total = 0; foreach($cart_items as $item) {?>
									<tr>
										<td>
											<?=$item['nama']; ?>
										</td>
										<td>
											<?=$item['jumlah']; ?>
										</td>
										<td>
											Rp. <?=number_format($item['harga_jual'],2,",","."); ?>
										</td>
										<td>
											Rp. <?=number_format($item['harga_jual'] * $item['jumlah'],2,",","."); ?>
										</td>
									</tr>
									<?php $total+= $item['harga_jual'] * $item['jumlah']; }?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="3">Total</th>
										<th>Rp. <?=number_format($total,2,",",".")?></th>
									</tr>
								</tfoot>
							</table>

						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.content -->

					<div class="box-footer">
						<div class="pull-right">
							<?=anchor('transaksi/confirm', '<i class="fa fa-chevron-right"></i>Confirm Payment', array("class"=>"btn btn-default"))?>
						</div>
					</div>
				</form>
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col-md-9 -->

		<div class="col-md-3">

			<div class="box" id="order-summary">
				<div class="box-header">
					<h3>Ringkasan pesanan</h3>
				</div>

				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td>subtotal</td>
								<th>Rp. <?=number_format($total,2,",",".")?></th>
							</tr>
							<tr>
								<td>Pengiriman</td>
								<th>Rp. <?=number_format($ongkos_kirim,2,",",".")?></th>
							</tr>
							<tr>
								<td>Kode unik</td>
								<th>Rp. <?=number_format($kode_unik,2,",",".")?></th>
							</tr>
							<tr class="total">
								<td>Total</td>
								<th>Rp. <?=number_format($total+$ongkos_kirim+$kode_unik,2,",",".");?></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /.col-md-3 -->
	</div>
	<!-- /.container -->
</div>