<div id="content">
	<div class="container">

		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Daftar Konfirmasi Pembayaran</li>
			</ul>
		</div>

		<div class="col-md-12" id="transfer-info">
			<div class="box">
				<div class="content">
					<div class="table-responsive">
							Silahkan transfer ke:
							<p>
								 bank BRI<br/>
								 Cabang perintis kemerdekaan<br/>
								 no rekening 341401031880536<br/>
								 atas nama Butik Fitrah
							</p> 
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.content -->
			</div>
			<!-- /.box -->
		</div>

		<div class="col-md-9">
			<div class="box">
				<form method="post" action="checkout4.html">
					<h1>Daftar Pesanan</h1>
					<div class="content">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Tanggal berakhir</th>
										<th>Jumlah dibayar</th>
										<th>Detail / konfirmasi</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($invoices as $invoice) {?>
									<tr>
										<td>
											<?=mysql_to_dmy_format($invoice['tanggal_berakhir']); ?>
										</td>
										<td>
											<?=$invoice['jumlah_dibayar']; ?>
										</td>
										<td>
											<?=anchor('transaksi/detail_payment/'.$invoice['id_pembayaran'], 'detail/konfirmasi'); ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.content -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->