<!-- *** FOOTER ***
 _________________________________________________________ -->
<div id="footer" data-animate="fadeInUp">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6">

				<h4>User section</h4>

				<ul>
					<li>
						<a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
					</li>
					<li>
						<?=anchor('user', 'Register')?>
					</li>
				</ul>

				<hr class="hidden-md hidden-lg hidden-sm">

			</div>
			<!-- /.col-md-3 -->

			<div class="col-md-3 col-sm-6">
				&nbsp;
			</div>
			<!-- /.col-md-3 -->

			<div class="col-md-3 col-sm-6">

				<h4>Alamat kami</h4>

				<p>
					<strong>Butik Fitrah.</strong>
					<br>Perintis kemerdekaan 3 Btn Hamzi
					<br>Blok  A13/5
					<br>Makassar
					<br>Sulawesi Selatan
				</p>

				<hr class="hidden-md hidden-lg">

			</div>
			<!-- /.col-md-3 -->

			<div class="col-md-3 col-sm-6">

				<h4>Stay in touch</h4>

				<p class="social">
					<a href="#" class="facebook external" data-animate-hover="shake">
						<i class="fa fa-facebook"></i>
					</a>
					<a href="#" class="twitter external" data-animate-hover="shake">
						<i class="fa fa-twitter"></i>
					</a>
					<a href="#" class="instagram external" data-animate-hover="shake">
						<i class="fa fa-instagram"></i>
					</a>
					<a href="#" class="gplus external" data-animate-hover="shake">
						<i class="fa fa-google-plus"></i>
					</a>
					<a href="#" class="email external" data-animate-hover="shake">
						<i class="fa fa-envelope"></i>
					</a>
				</p>

			</div>
			<!-- /.col-md-3 -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->
</div>
<!-- /#footer -->

<!-- *** FOOTER END *** -->




<!-- *** COPYRIGHT ***
 _________________________________________________________ -->
<div id="copyright">
	<div class="container">
		<div class="col-md-6">
			<p class="pull-left">© 2015 Your name goes here.</p>

		</div>
		<div class="col-md-6">
			<p class="pull-right">Template by
				<a href="https://bootstrapious.com/e-commerce-templates">Bootstrapious</a> &
				<a href="https://fity.cz">Fity</a>
				<!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
			</p>
		</div>
	</div>
</div>
<!-- *** COPYRIGHT END *** -->



</div>
<!-- /#all -->




<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
<script src="<?php echo base_url('assets/js/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookie.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-hover-dropdown.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/front.js'); ?>"></script>

<?php if( ($this->session->flashdata('success') !=null) &&  ($this->session->flashdata('success')=='register success') ) { ?>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.25.0/dist/sweetalert2.all.min.js"></script>

	<script type="text/javascript">
		$(function() {
			swal({
			  title: 'Berhasil!',
			  text: 'Register berhasil..',
			  type: 'success',
			  confirmButtonText: 'Ok'
			})
		});
	</script>
<?php } ?>

<?php if( ($this->session->flashdata('success') !=null) &&  ($this->session->flashdata('success')=='login success') ) { ?>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.25.0/dist/sweetalert2.all.min.js"></script>

	<script type="text/javascript">
		$(function() {
			swal({
			  title: 'Welcome!',
			  text: 'Login berhasil. Selamat datang <?=$this->session->userdata('nama_lengkap')?>',
			  type: 'success',
			  confirmButtonText: 'Ok'
			})
		});
	</script>
<?php } ?>

<?php if( ($this->session->flashdata('error') !=null) &&  ($this->session->flashdata('error')=='login gagal') ) { ?>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.25.0/dist/sweetalert2.all.min.js"></script>

	<script type="text/javascript">
		$(function() {
			swal({
			  title: 'Error!',
			  text: 'Login gagal.',
			  type: 'error',
			  confirmButtonText: 'Ok'
			})
		});
	</script>
<?php } ?>

<?php if( ($this->uri->segment(1) == 'transaksi') &&  ($this->uri->segment(2) == 'checkOut') ) { ?>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.25.0/dist/sweetalert2.all.min.js"></script>

	<script type="text/javascript">
		$(function() {
			swal({
			  title: 'Perhatian',
			  text: 'Lakukan transfer dalam 24 jam. Jika tidak, transaksi anda akan kedaluarsa dan anda harus melakukan order ulang',
			  type: 'info',
			  confirmButtonText: 'Ok'
			})
		});
	</script>
<?php } ?>
</body>

</html>
