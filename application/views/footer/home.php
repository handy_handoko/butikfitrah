<!-- footer-top -->
<div class="w3agile-ftr-top">
  <div class="container">
    <div class="ftr-toprow">
      <div class="col-md-4 ftr-top-grids">
        <div class="ftr-top-left">
          <i class="fa fa-truck" aria-hidden="true"></i>
        </div>
        <div class="ftr-top-right">
          <h4>FAST DELIVERY</h4>
          <p>Pengiriman barang lebih cepat</p>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="col-md-4 ftr-top-grids">
        <div class="ftr-top-left">
          <i class="fa fa-user" aria-hidden="true"></i>
        </div>
        <div class="ftr-top-right">
          <h4>CUSTOMER CARE</h4>
          <p>Pelayanan yang ramah</p>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="col-md-4 ftr-top-grids">
        <div class="ftr-top-left">
          <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
        </div>
        <div class="ftr-top-right">
          <h4>GOOD QUALITY</h4>
          <p>Barang yang berkualitas</p>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>
<!-- //footer-top -->
<!-- subscribe -->
<div class="subscribe">
  <div class="container">
    <div class="col-md-6 social-icons w3-agile-icons">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
        <li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
        <li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
        <li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
        <li><a href="#" class="fa fa-rss icon rss"> </a></li>
      </ul>
      <ul class="apps">
        <li><h4>Download Our app : </h4> </li>
        <li><a href="#" class="fa fa-apple"></a></li>
        <li><a href="#" class="fa fa-windows"></a></li>
        <li><a href="#" class="fa fa-android"></a></li>
      </ul>
    </div>
    <div class="col-md-6 subscribe-right">
      <h4>Sign up for email and get 25%off!</h4>
      <form action="#" method="post">
        <input type="text" name="email" placeholder="Enter your Email..." required="">
        <input type="submit" value="Subscribe">
      </form>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
<!-- //subscribe -->
<!-- footer -->
<div class="footer">
  <div class="container">
    <div class="footer-info w3-agileits-info">
      <div class="col-md-4 address-left agileinfo">
        <div class="footer-logo header-logo">
          <h2><a href="<?php echo base_url(); ?>"><span>I</span>sam <i></i></a></h2>
          <h6>Your stores. Your place.</h6>
        </div>
        <ul>
          <li><i class="fa fa-map-marker"></i> Jl Sultan Hasanuddin no 106.</li>
          <li><i class="fa fa-mobile"></i> +62 812356-713897 </li>
          <li><i class="fa fa-phone"></i> +62 11-612787 </li>
          <li><i class="fa fa-envelope-o"></i> <a href="mailto:tugasakhir@gmail.com"> tugasakhir@gmail.com</a></li>
        </ul>
      </div>
      <div class="col-md-8 address-right">
        <div class="col-md-4 footer-grids">
          <h3>Company</h3>
          <ul>
            <li><a href="<?php echo base_url('assets/about.html'); ?>">About Us</a></li>
            <li><a href="<?php echo base_url('assets/marketplace.html'); ?>">Marketplace</a></li>
            <li><a href="<?php echo base_url('assets/values.html'); ?>">Core Values</a></li>
            <li><a href="<?php echo base_url('assets/privacy.html'); ?>">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-md-4 footer-grids">
          <h3>Services</h3>
          <ul>
            <li><a href="<?php echo base_url('assets/contact.html'); ?>">Contact Us</a></li>
            <li><a href="<?php echo base_url('assets/login.html'); ?>">Returns</a></li>
            <li><a href="<?php echo base_url('assets/faq.html'); ?>">FAQ</a></li>
            <li><a href="<?php echo base_url('assets/sitemap.html'); ?>">Site Map</a></li>
            <li><a href="<?php echo base_url('assets/login.html'); ?>">Order Status</a></li>
          </ul>
        </div>
        <div class="col-md-4 footer-grids">
          <h3>Payment Methods</h3>
          <ul>
            <li><i class="fa fa-laptop" aria-hidden="true"></i> Net Banking</li>
            <li><i class="fa fa-money" aria-hidden="true"></i> Cash On Delivery</li>
            <li><i class="fa fa-pie-chart" aria-hidden="true"></i>EMI Conversion</li>
            <li><i class="fa fa-gift" aria-hidden="true"></i> E-Gift Voucher</li>
            <li><i class="fa fa-credit-card" aria-hidden="true"></i> Debit/Credit Card</li>
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<!-- //footer -->
<div class="copy-right">
  <div class="container">
    <p>© 2017 Isam Store . All rights reserved | Design by <a href="http://w3layouts.com"> Selly.</a></p>
  </div>
</div>
<!-- cart-js -->
<!-- <script src="<?php echo base_url('assets/js/minicart.js'); ?>"></script> -->
<script>
      w3ls.render();

      w3ls.cart.on('w3sb_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
          items = this.items();

          for (i = 0, len = items.length; i < len; i++) {
            items[i].set('shipping', 0);
            items[i].set('shipping2', 0);
          }
        }
      });
  </script>
<!-- //cart-js -->
<!-- countdown.js -->
<script src="<?php echo base_url('assets/js/jquery.knob.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.throttle.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.classycountdown.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      $('#countdown1').ClassyCountdown({
        end: '1388268325',
        now: '1387999995',
        labels: true,
        style: {
          element: "",
          textResponsive: .5,
          days: {
            gauge: {
              thickness: .10,
              bgColor: "rgba(0,0,0,0)",
              fgColor: "#1abc9c",
              lineCap: 'round'
            },
            textCSS: 'font-weight:300; color:#fff;'
          },
          hours: {
            gauge: {
              thickness: .10,
              bgColor: "rgba(0,0,0,0)",
              fgColor: "#05BEF6",
              lineCap: 'round'
            },
            textCSS: ' font-weight:300; color:#fff;'
          },
          minutes: {
            gauge: {
              thickness: .10,
              bgColor: "rgba(0,0,0,0)",
              fgColor: "#8e44ad",
              lineCap: 'round'
            },
            textCSS: ' font-weight:300; color:#fff;'
          },
          seconds: {
            gauge: {
              thickness: .10,
              bgColor: "rgba(0,0,0,0)",
              fgColor: "#f39c12",
              lineCap: 'round'
            },
            textCSS: ' font-weight:300; color:#fff;'
          }

        },
        onEndCallback: function() {
          console.log("Time out!");
        }
      });
    });
  </script>
<!-- //countdown.js -->
<!-- menu js aim -->
<script src="<?php echo base_url('assets/js/jquery.menu-aim.js'); ?>"> </script>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script> <!-- Resource jQuery -->
<!-- //menu js aim -->
<!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
