<header>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="navigation">
			<div class="container">
				<div class="navbar-header">
					<div class="navbar-brand">
						<?=anchor("/", "<h1><span>Isam</span>Commerce</h1>")?>
					</div>
				</div>

				<div class="navbar-collapse collapse">
					<div class="menu">
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><?=anchor("", " Home")?></li>
							<?php if($this->session->userdata('role') != null){ ?>
								<li role="presentation"><?=anchor("transaksi/cart", "<i class='fa fa-shopping-cart fa-fw'></i> Cart")?></li>
								<li role="presentation"><?=anchor("transaksi/confirm", "<i class='fa fa-credit-card fa-fw'></i> Confirm Payment")?></li>
								<li role="presentation"><?=anchor("user/edit_profile", "<i class='fa fa-user fa-fw'></i> Profile")?></li>
								<li role="presentation"><?=anchor("user/logout", "<i class='fa fa-sign-out fa-fw'></i> Logout")?></li>
							<?php } else {?>
								<li role="presentation"><?=anchor("user", "<i class='fa fa-user fa-fw'></i> Login")?></li>
							<?php } ?>
							<!-- <li role="presentation"><a href="index.html" class="active">Home</a></li>
							<li role="presentation"><a href="about.html">About Us</a></li>
							<li role="presentation"><a href="services.html">Services</a></li>
							<li role="presentation"><a href="portfolio.html">Portfolio</a></li>
							<li role="presentation"><a href="blog.html">Blog</a></li>
							<li role="presentation"><a href="contact.html">Contact</a></li> -->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>
<!-- Navigation -->
<!-- <form action="<?=site_url('alkes/search')?>" class="navbar-form" >
	<div class="form-group">
		<input type="text" class="form-control" placeholder="Search" name="nama_alkes">
	</div>
	<button type="submit" class="btn btn-default">Cari</button>
</form> -->