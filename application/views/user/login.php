<div id="content">
	<div class="container">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>Daftar / Login</li>
			</ul>
		</div>
		<div class="col-md-6">
			<div class="box">
				<h1>Daftar</h1>
				<?=form_open('user/register')?>
					<div class="form-group">
						<label for="nama_lengkap">Nama</label>
						<?=form_input('nama_lengkap', '', array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Nama Lengkap'));?>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<?=form_input('email', '', array('class'=>'form-control', 'id'=>'email', 'type'=>'email', 'placeholder'=>'Email'));?>
					</div>
					<div class="form-group">
						<label for="id_provinsi">Provinsi</label>
						<?=form_dropdown('id_provinsi', $provinsi, null, array('class'=>'form-control', 'id'=>'id_provinsi'));?>
					</div>
					<div class="form-group">
						<label for="id_kota">Kota</label>
						<?=form_dropdown('id_kota', $kota, null, array('class'=>'form-control', 'id'=>'id_kota', 'placeholder'=>'Nama lengkap'));?>
					</div>
					<div class="form-group">
						<label for="telepon">Telepon</label>
						<?=form_input('telepon', null, array('class'=>'form-control', 'id'=>'telepon', 'placeholder'=>'Telepon'));?>
					</div>
					<div class="form-group">
						<label for="telepon">Kecamatan</label>
						<?=form_input('kecamatan', null, array('class'=>'form-control', 'id'=>'kecamatan', 'placeholder'=>'Kecamatan'));?>
					</div>
					<div class="form-group">
						<label for="telepon">Kelurahan</label>
						<?=form_input('kelurahan', null, array('class'=>'form-control', 'id'=>'kelurahan', 'placeholder'=>'Kelurahan'));?>
					</div>
					<div class="form-group">
						<label for="telepon">RT</label>
						<?=form_input('RT', null, array('class'=>'form-control', 'id'=>'RT', 'placeholder'=>'RT'));?>
					</div>
					<div class="form-group">
						<label for="telepon">RW</label>
						<?=form_input('RW', null, array('class'=>'form-control', 'id'=>'RW', 'placeholder'=>'RW'));?>
					</div>
					<div class="form-group">
						<label for="kodepos">Kodepos</label>
						<?=form_input('kodepos', '', array('class'=>'form-control', 'id'=>'kodepos', 'placeholder'=>'Kodepos'));?>
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<?=form_input('alamat', '', array('class'=>'form-control', 'id'=>'alamat', 'placeholder'=>'Alamat'));?>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<?=form_password('password', '', array('class'=>'form-control', 'id'=>'password', 'placeholder'=>'Password'));?>
					</div>
					<div class="form-group">
						<label for="retypepassword">Ulangi Password</label>
						<?=form_password('retypepassword', '', array('class'=>'form-control', 'id'=>'retypepassword', 'placeholder'=>'Ulangi Password'));?>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-user-md"></i> Register</button>
					</div>
					<?=form_close();?>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
				<h1>Login</h1>
				<?= form_open('user/login'); ?>
					<div class="form-group">
						<label for="email">Email</label>
						<?=form_input('email', null, array("class"=>"form-control", "id"=>"email"));?>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<?=form_password('password', null, array("class"=>"form-control", "id"=>"password"));?>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-sign-in"></i> Log in
						</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /#content -->
