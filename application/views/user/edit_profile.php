<div class="container" id="forsale" class="content">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">update profile</h3>
				</div>
				<div class="panel-body">
					<?=form_open('user/update'); ?>
						<fieldset>
							<?=form_hidden('id_user', $this->session->userdata('id_user'));?>
							<div class="form-group">
								<?=form_input('email', $profile['email'], array('class'=>'form-control', 'id'=>'email', 'type'=>'email', 'placeholder'=>'Email'));?>
							</div>
							<div class="form-group">
								<?=form_input('username', $profile['username'], array('class'=>'form-control', 'id'=>'username', 'placeholder'=>'Username'));?>
							</div>
							<div class="form-group">
								<?=form_password('password', '', array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Password'));?>
							</div>
							<div class="form-group">
								<?=form_password('retypepassword', '', array('class'=>'form-control', 'id'=>'email', 'type'=>'email', 'placeholder'=>'Ulangi Password'));?>
							</div>
							<div class="form-group">
								<?=form_input('nama_lengkap', $profile['nama_lengkap'], array('class'=>'form-control', 'id'=>'nama_lengkap', 'placeholder'=>'Nama lengkap'));?>
							</div>
							<div class="form-group">
								<?=form_input('alamat', $profile['alamat'], array('class'=>'form-control', 'id'=>'alamat', 'placeholder'=>'Alamat'));?>
							</div>
							<div class="form-group">
								<?=form_dropdown('id_provinsi', $provinsi, null, array('class'=>'form-control', 'id'=>'id_provinsi'));?>
							</div>
							<div class="form-group">
								<?=form_dropdown('id_kota', $kota, null, array('class'=>'form-control', 'id'=>'id_kota', 'placeholder'=>'Nama lengkap'));?>
							</div>
							<div class="form-group">
								<?=form_input('telepon', $profile['telepon'], array('class'=>'form-control', 'id'=>'telepon', 'placeholder'=>'Telepon'));?>
							</div>
							<div class="form-group">
								<?=form_input('kecamatan', $profile['kecamatan'], array('class'=>'form-control', 'id'=>'kecamatan', 'placeholder'=>'Kecamatan'));?>
							</div>
							<div class="form-group">
								<?=form_input('kelurahan', $profile['kelurahan'], array('class'=>'form-control', 'id'=>'kelurahan', 'placeholder'=>'Telepon'));?>
							</div>
							<div class="form-group">
								<?=form_input('RT', $profile['RT'], array('class'=>'form-control', 'id'=>'RT', 'placeholder'=>'RT'));?>
							</div>
							<div class="form-group">
								<?=form_input('RW', $profile['RW'], array('class'=>'form-control', 'id'=>'RW', 'placeholder'=>'RW'));?>
							</div>
							<div class="form-group">
								<?=form_input('kodepos', $profile['kodepos'], array('class'=>'form-control', 'id'=>'kodepos', 'placeholder'=>'Kodepos'));?>
							</div>
							<!-- Change this to a button or input when using this as a form -->
							<?= form_button(array(
								'type'	 => 'submit',
								'class'	 => 'btn btn-lg btn-primary btn-block',
								'content'=> 'Update'
								)); 
							?>
						</fieldset>
					<?=form_close()?>
				</div>
			</div>
		</div>
	</div>
</div>