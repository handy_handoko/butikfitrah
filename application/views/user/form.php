<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?></title>

	<!-- Bootstrap Core CSS -->
	<link href=<?=base_url("/assets/vendor/bootstrap/css/bootstrap.min.css")?> rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.css")?> rel="stylesheet">

	<!-- DataTables CSS -->
	<link href=<?=base_url("/assets/vendor/datatables-plugins/dataTables.bootstrap.css")?> rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href=<?=base_url("/assets/vendor/datatables-responsive/dataTables.responsive.css")?> rel="stylesheet">

	<link href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">

  <link rel="stylesheet" href="<?=base_url('/assets/css/bootstrap-datepicker.min.css')?>" />

	<!-- Custom CSS -->
	<link href=<?=base_url("/assets/dist/css/sb-admin-2.css")?> rel="stylesheet">

	<!-- Custom Fonts -->
	<link href=<?=base_url("/assets/vendor/font-awesome/css/font-awesome.min.css")?> rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<div id="wrapper">
		<?php $this->load->view("admin_menu");?>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3">
					<div class="panel panel-default">
						<!-- /.panel-heading -->
						<div class="panel-body">
							<?=form_open('user/save'); ?>
								<fieldset>
									<div class="form-group">
										<?=form_input('email', isset($admin['email'])?$admin['email']:'', array('class'=>'form-control', 'id'=>'email', 'type'=>'email', 'placeholder'=>'Email'));?>
									</div>
									<div class="form-group">
										<?=form_input('username', isset($admin['username'])?$admin['username']:'', array('class'=>'form-control', 'id'=>'username', 'placeholder'=>'Username'));?>
									</div>
									<div class="form-group">
										<?=form_password('password', '', array('class'=>'form-control', 'id'=>'email', 'placeholder'=>'Password'));?>
									</div>
									<div class="form-group">
										<?=form_password('retypepassword', '', array('class'=>'form-control', 'id'=>'email', 'type'=>'email', 'placeholder'=>'Ulangi Password'));?>
									</div>
									<div class="form-group">
										<?=form_input('nama_lengkap', isset($admin['nama_lengkap'])?$admin['nama_lengkap']:'', array('class'=>'form-control', 'id'=>'nama_lengkap', 'placeholder'=>'Nama lengkap'));?>
									</div>
									<!-- Change this to a button or input when using this as a form -->
									<?= form_button(array(
										'type'	 => 'submit',
										'class'	 => 'btn btn-lg btn-primary btn-block',
										'content'=> 'Submit'
										)); 
									?>
								</fieldset>
							<?=form_close()?>
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src=<?=base_url("/assets/vendor/jquery/jquery.min.js")?>></script>

	<!-- Bootstrap Core JavaScript -->
	<script src=<?=base_url("/assets/vendor/bootstrap/js/bootstrap.min.js")?>></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.js")?>></script>
	<!-- DataTables JavaScript -->
	<script src=<?=base_url("/assets/vendor/datatables/js/jquery.dataTables.min.js")?>></script>
	<script src=<?=base_url("/assets/vendor/datatables-plugins/dataTables.bootstrap.min.js")?>></script>
	<script src=<?=base_url("/assets/vendor/datatables-responsive/dataTables.responsive.js")?>></script>
	<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
	
  <script type="text/javascript" src="<?=base_url('/assets/js/moment.js')?>"></script>
  <script type="text/javascript" src="<?=base_url('/assets/js/bootstrap-datepicker.min.js')?>"></script>

	<!-- Custom Theme JavaScript -->
	<script src=<?=base_url("/assets/dist/js/sb-admin-2.js")?>></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			dom: 'Bfrtip',
			buttons: [
				'pdf',
				'excel',
				'print'
			]
		});
	});
	</script>
</body>
</html>