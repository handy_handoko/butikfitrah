<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<?=anchor("alkes", $this->config->item('app_name'), array("class"=>"navbar-brand")) ?>
	</div>
	<!-- /.navbar-header -->

	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-alerts">
				<li>
					<a href="#">
						<div>
							<i class="fa fa-money fa-fw"></i> <div id="payment" style="display: inline">Loading</div>
						</div>
					</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="#">
						<div>
							<i class="fa fa-cart-plus fa-fw"></i> <div id="transaction" style="display: inline">Loading</div>
						</div>
					</a>
				</li>
			</ul>
			<!-- /.dropdown-alerts -->
		</li>
		<!-- /.dropdown -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li>
					<?=anchor("admin/logout", "<i class='fa fa-sign-out fa-fw'></i> Logout") ?>
				</li>
			</ul>
			<!-- /.dropdown-user -->
		</li>
		<!-- /.dropdown -->
	</ul>
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li class="sidebar-search">
					<div class="input-group custom-search-form">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
					</div>
					<!-- /input-group -->
				</li>
				<li>
					<?=anchor("admin/dashboard", "<i class='fa fa-dashboard fa-fw'></i> Dashboard")?>
				</li>
				<li>
					<?=anchor("produk", "<i class='fa fa-table fa-fw'></i> Produk")?>
				</li>
				<li>
					<?=anchor("kategori", "<i class='fa fa-table fa-fw'></i> Kategori")?>
				</li>
				<li>
					<?=anchor("transaksi/browse_payment", "<i class='fa fa-edit fa-fw'></i> Pembayaran")?>
				</li>
				<li>
					<?=anchor("transaksi/browse_transaction", "<i class='fa fa-edit fa-fw'></i> Transaksi")?>
				</li>
				<li>
					<?=anchor("report/penjualan", "<i class='fa fa-edit fa-fw'></i> Report Penjualan")?>
				</li>
				<li>
					<?=anchor("post", "<i class='fa fa-edit fa-fw'></i> Blog Post")?>
				</li>
				<li>
					<?=anchor("admin/browse", "<i class='fa fa-edit fa-fw'></i> Admin Management")?>
				</li>
				<li>
					<?=anchor("user/browse", "<i class='fa fa-edit fa-fw'></i> User Management")?>
				</li>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>
<script src=<?=site_url("../assets/vendor/jquery/jquery.min.js")?>></script>
<script type="text/javascript">
	$(function() {
		$.post( '<?=site_url("transaksi/getNotif")?>' , function( data ) {
			console.log('x');
			$('#payment').html(data.payment + " pembayaran belum terverifikasi.");
			$('#transaction').html(data.transaction + " transaksi belum di proses.");
		});
	});
</script>