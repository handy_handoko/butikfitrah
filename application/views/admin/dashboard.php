<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?></title>

    <!-- Bootstrap Core CSS -->
    <link href=<?=base_url("/assets/vendor/bootstrap/css/bootstrap.min.css")?> rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.css")?> rel="stylesheet">

    <!-- Custom CSS -->
    <link href=<?=base_url("/assets/dist/css/sb-admin-2.css")?> rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=<?=base_url("/assets/vendor/font-awesome/css/font-awesome.min.css")?> rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

		<?php $this->load->view("admin_menu");?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?=$total_penjualan?></div>
                                    <div>Penjualan bulan ini</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=site_url('report/penjualan')?>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-gift fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?=$total_produk?></div>
                                    <div>Total jenis barang</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=site_url('produk/')?>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-ban fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?=$total_produk_habis?></div>
                                    <div>Produk habis</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=site_url('produk/')?>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?=form_open('')?>
                            <div class="form-group row">
                                <div class='col-md-3'>
                                    <?=form_input('start_date', '', array('class'=>'form-control col-md-3 datepicker', 'required'=>''));?>
                                </div>
                                <div class='col-md-3'>
                                    <?=form_input('end_date', '', array('class'=>'form-control col-md-3 datepicker', 'required'=>''));?>
                                </div>
                                <button type="submit" class="btn btn-default">Filter</button>
                            </div>
                            <?=form_close()?>
                            <table border="1" width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>Nama Barang</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($data_harian as $no=>$d) { ?>
                                    <tr>
                                        <td><?=$no+1?></td>
                                        <td><?=$d['tanggal_penjualan']?></td>
                                        <td><?=$d['nama_produk']?></td>
                                        <td><?=$d['total_penjualan']?></td>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>Nama Barang</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <table border="1" width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>Nama Barang</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($data_mingguan as $no=>$d) { ?>
                                    <tr>
                                        <td><?=$no+1?></td>
                                        <td><?=$d['tanggal_penjualan']?></td>
                                        <td><?=$d['nama_produk']?></td>
                                        <td><?=$d['total_penjualan']?></td>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>Nama Barang</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- /.table-responsive -->
                            <?php if($date == array()){ ?>
                                <?=anchor("report/reportpenjualan", "print", array("target"=>"_blank", 'class'=>'btn btn-primary'))?>
                                <?=anchor("report/pdfpenjualan", "pdf", array("target"=>"_blank", 'class'=>'btn btn-primary'))?>
                            <?php } else { ?>
                                <?=anchor("report/reportpenjualan?start=".$date['start_date'].'&end='.$date['end_date'], "print", array("target"=>"_blank", 'class'=>'btn btn-primary'))?>
                                <?=anchor("report/pdfpenjualan?start=".$date['start_date'].'&end='.$date['end_date'], "pdf", array("target"=>"_blank", 'class'=>'btn btn-primary'))?>
                            <?php } ?>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src=<?=base_url("/assets/vendor/jquery/jquery.min.js")?>></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=<?=base_url("/assets/vendor/bootstrap/js/bootstrap.min.js")?>></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.js")?>></script>

    <!-- Custom Theme JavaScript -->
    <script src=<?=base_url("/assets/dist/js/sb-admin-2.js")?>></script>

</body>

</html>
