<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?=$this->config->item('app_name') .' - '. $this->config->item('company_name') ?></title>

	<!-- Bootstrap Core CSS -->
	<link href=<?=base_url("/assets/vendor/bootstrap/css/bootstrap.min.css")?> rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.css")?> rel="stylesheet">

	<!-- Custom CSS -->
	<link href=<?=base_url("/assets/dist/css/sb-admin-2.css")?> rel="stylesheet">

	<!-- Custom Fonts -->
	<link href=<?=base_url("/assets/vendor/font-awesome/css/font-awesome.min.css")?> rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<div id="wrapper">
		<?php $this->load->view("admin_menu");?>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Data Produk</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Form Produk
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<?=form_open_multipart('produk/save'); ?>
									<?php if(isset($data['id_produk']))
										echo form_hidden('id_produk', $data['id_produk']);?>
										<div class="form-group">
											<label>Nama Produk</label>
											<?=form_input('nama_produk', isset($data['nama_produk'])?$data['nama_produk']:'', array('class'=>'form-control', 'id'=>'nama_produk','placeholder'=>'Masukkan nama produk'));?>
										</div>
										<div class="form-group">
											<label>Kategori</label>
											<?=form_dropdown('id_kategori', $kategori, isset($data['id_kategori'])?$data['id_kategori']:null, array('class'=>'form-control', 'id'=>'id_kategori','placeholder'=>'Masukkan nama produk'));?>
										</div>
										<div class="form-group">
											<label>Harga jual</label>
											<?=form_input('harga_jual', isset($data['harga_jual'])?$data['harga_jual']:'', array('class'=>'form-control', 'id'=>'harga_jual'));?>
										</div>
										<div class="form-group">
											<label>Harga modal</label>
											<?=form_input('harga_modal', isset($data['harga_modal'])?$data['harga_modal']:'', array('class'=>'form-control', 'id'=>'harga_modal'));?>
										</div>
										<div class="form-group">
											<label>Stok</label>
											<?=form_input('stok', isset($data['stok'])?$data['stok']:'', array('class'=>'form-control', 'id'=>'stok'));?>
										</div>
										<div class="form-group">
											<label>Berat</label>
											<?=form_input('berat', isset($data['berat'])?$data['berat']:'', array('class'=>'form-control', 'id'=>'berat'));?>
										</div>
										<div class="form-group">
											<label>Gambar</label>
											<?=form_upload('gambar[]', null, array('class'=>'form-control', 'id'=>'gambar', 'multiple'=>''));?>
										</div>
										<div class="form-group">
											<label>Uraian</label>
											<?=form_textarea('deskripsi', isset($data['deskripsi'])?$data['deskripsi']:'', array('class'=>'form-control', 'id'=>'deskripsi'));?>
										</div>
										<button type="submit" class="btn btn-success">Submit</button>
										<button type="reset" class="btn btn-default">Reset</button>
									<?=form_close()?>
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src=<?=base_url("/assets/vendor/jquery/jquery.min.js")?>></script>

	<!-- Bootstrap Core JavaScript -->
	<script src=<?=base_url("/assets/vendor/bootstrap/js/bootstrap.min.js")?>></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src=<?=base_url("/assets/vendor/metisMenu/metisMenu.min.js")?>></script>

	<!-- Custom Theme JavaScript -->
	<script src=<?=base_url("/assets/dist/js/sb-admin-2.js")?>></script>
	
	<script>
	$(document).ready(function() {
		$('#disewakan').click(function(){
			$('.sewa').removeAttr('disabled');
		});
	});
	</script>
</body>

</html>

